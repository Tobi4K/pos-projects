package orbit;

import java.util.*;
import java.util.stream.Collectors;

public class Orbits {

    private final Map<String, String> predecessorMap;

    public Orbits(String[] input) {
        predecessorMap = Arrays.stream(input)
                .map(entry -> entry.split("\\)"))
                .filter(entry -> entry.length == 2)
                .collect(Collectors.toMap(entry -> entry[1], entry -> entry[0]));
    }

    public Map<String, String> getPredecessor() {
        /* Copy with streaming API
        return predecessorMap
                .entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        */
        return new HashMap<>(predecessorMap);
    }

    public String getRoot() {
        var possibleRoots = predecessorMap.values()
                .stream()
                .filter(value -> !predecessorMap.containsKey(value))
                .collect(Collectors.toList());

        if (possibleRoots.size() != 1)
            throw new IllegalStateException("No root can be determined");

        return possibleRoots.get(0);
    }

    public int countTotalPredecessors() {
        int totalPredecessors = 0;
        var rootElement = getRoot();

        for (var key : predecessorMap.keySet()) {
            var currentPredecessor = predecessorMap.get(key);
            totalPredecessors++;

            while (!currentPredecessor.equals(rootElement)) {
                totalPredecessors++;
                currentPredecessor = predecessorMap.get(currentPredecessor);
            }
        }

        return totalPredecessors;
    }
}
