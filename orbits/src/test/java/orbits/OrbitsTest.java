package orbits;

import orbit.Orbits;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class OrbitsTest {

    @Test
    //        G - H         J - K
    //       /              /
    //    COM - B - C - D - E - F
    //               \
    //                 I
    void testConstructor() {
        String [] in = {"COM)B", "B)C", "C)D", "D)E", "E)F", "B)G", "G)H",
                "D)I", "E)J", "J)K"};
        Orbits o = new Orbits(in);

        Map<String, String> expected = Map.of("B" ,"COM",
                "C", "B",
                "D", "C",
                "E", "D",
                "F", "E",
                "G", "B",
                "H", "G",
                "I", "D",
                "J", "E",
                "K", "J");
        assertThat(o.getPredecessor()).isEqualTo(expected);
    }

    @Test
    //           G - H       J - K
    //          /           /
    //    COM - B - C - D - E - F
    //                   \
    //                    I
    // Clearly Root: COM
    void testRoot_ClearlyRoot() {
        String [] in = {"COM)B", "B)C", "C)D", "D)E", "E)F", "B)G", "G)H",
                "D)I", "E)J", "J)K"};
        Orbits o = new Orbits(in);
        assertThat(o.getRoot()).isEqualTo("COM");
    }

    @Test
    void testRoot_ClearlyRoot_File() throws Exception {
        String [] in = Files.readAllLines(Paths.get("input_orbits.txt"))
                .toArray(new String[0]);
        Orbits o = new Orbits(in);
        assertThat(o.getRoot()).isEqualTo("COM");
    }

    @Test
    //           G - H       J - K - L
    //          /
    //    COM - B - C - D - E - F
    //                   \
    //                    I
    // No clearly root (COM, J)
    void testRoot_NoClearlyRoot() {
        String [] in = {"COM)B", "B)C", "C)D", "D)E", "E)F", "B)G", "G)H",
                "D)I", "J)K", "K)L"};
        Orbits o = new Orbits(in);
        Assertions.assertThatThrownBy(() -> o.getRoot())
                .isInstanceOf(IllegalStateException.class)
                .hasMessageContaining("No root");
    }

    @Test
    //           G - H      J - K - L
    //          /           /
    //    COM - B - C - D - E - F
    //                   \
    //                    I
    // Number of Predecessors:
    // B: 1, C: 2, D: 3, E: 4, F: 5, I: 4, G: 2, H: 3, J: 5, K: 6, L: 7
    // Summe: 42

    void countTotalPredecessors() {
        String [] in = {"COM)B", "B)C", "C)D", "D)E", "E)F", "B)G", "G)H",
                "D)I", "E)J", "J)K", "K)L"};
        Orbits o = new Orbits(in);
        assertThat(o.countTotalPredecessors()).isEqualTo(42);
    }

    @Test
    void countTotalPredecessors_File() throws Exception {
        String [] in = Files.readAllLines(Paths.get("input_orbits.txt"))
                .toArray(new String[0]);
        Orbits o = new Orbits(in);
        assertThat(o.countTotalPredecessors()).isEqualTo(295936);
    }
}
