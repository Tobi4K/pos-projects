package at.tobiasnolz.personinterest.persistence;

import at.tobiasnolz.personinterest.domain.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, Integer> {
}
