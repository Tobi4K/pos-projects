package at.tobiasnolz.personinterest.persistence;

import at.tobiasnolz.personinterest.domain.Interest;
import at.tobiasnolz.personinterest.domain.Person;
import at.tobiasnolz.personinterest.domain.Sex;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
public class DatabaseInitializer implements CommandLineRunner {

    private final InterestRepository interestRepository;
    private final PersonRepository personRepository;

    @Override
    public void run(String... args) {
        var persons = List.of(
                new Person("Tobias", "Nolz", LocalDate.of(2002, 10, 9), Sex.MALE),
                new Person("Alena", "Liebhart", LocalDate.of(2003, 8, 23), Sex.FEMALE),
                new Person("Benedikt", "Schrauber", LocalDate.of(2002, 10, 9), Sex.DIVERSE)
        );

        var interests = List.of(
                new Interest("Stocks"),
                new Interest("Crypto"),
                new Interest("Staking"),
                new Interest("Liquidity Mining"),
                new Interest("Lending"),
                new Interest("Shorting")
        );

        interests = interestRepository.saveAll(interests);
        persons = personRepository.saveAll(persons);

        var personToAddInterests = persons.get(0);
        personToAddInterests.addAllInterest(
                interests.stream()
                        .skip(interests.size() - 3)
                        .collect(Collectors.toList())
        );

        personRepository.save(personToAddInterests);
    }
}
