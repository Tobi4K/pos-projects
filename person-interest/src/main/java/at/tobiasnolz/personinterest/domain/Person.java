package at.tobiasnolz.personinterest.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Person {

    @Id
    @GeneratedValue
    private Integer id;

    @Pattern(regexp = "^[A-Z].{0,29}$", message = "Does not match pattern (30 chars, first letter uppercase)")
    private String firstName;

    @Pattern(regexp = "^[A-Z].{0,29}$", message = "Does not match pattern (30 chars, first letter uppercase)")
    private String lastName;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @PastOrPresent
    @NotNull
    private LocalDate dateOfBirth;

    @ManyToMany
    @Size(max = 3)
    private Set<Interest> interests = new HashSet<>();

    @NotNull
    private Sex sex;

    public Person(String firstName, String lastName, LocalDate dateOfBirth, Sex sex) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.sex = sex;
    }

    public String getInterestString() {
        return this.interests.stream()
                .map(Interest::getDescription)
                .collect(Collectors.joining(", "));
    }

    public void addInterest(Interest interest) {
        interests.add(interest);
    }

    public void addAllInterest(List<Interest> interests) {
        this.interests.addAll(interests);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return id.equals(person.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
