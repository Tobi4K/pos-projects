package at.tobiasnolz.personinterest.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Interest {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(unique = true)
    private String description;

    public Interest(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Interest interest = (Interest) o;
        return id.equals(interest.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
