package at.tobiasnolz.personinterest.domain;

public enum Sex {
    MALE, FEMALE, DIVERSE;
}
