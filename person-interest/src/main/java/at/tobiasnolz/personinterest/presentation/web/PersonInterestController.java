package at.tobiasnolz.personinterest.presentation.web;

import at.tobiasnolz.personinterest.domain.Person;
import at.tobiasnolz.personinterest.domain.Sex;
import at.tobiasnolz.personinterest.persistence.InterestRepository;
import at.tobiasnolz.personinterest.persistence.PersonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/persons")
@RequiredArgsConstructor
public class PersonInterestController {

    private final InterestRepository interestRepository;
    private final PersonRepository personRepository;

    @GetMapping()
    public String getOverview(Model model) {
        var persons = personRepository.findAll();
        model.addAttribute("persons", persons);
        return "overview";
    }

    @GetMapping("/new")
    public String getNewPerson(Model model) {
        model.addAttribute("interests", interestRepository.findAll());
        model.addAttribute("sexes", Sex.values());
        model.addAttribute("person", new Person());
        return "new";
    }

    @PostMapping("/new")
    public String addNewGradeEntry(@Valid @ModelAttribute("person") Person person,
                                   BindingResult bindingResult,
                                   Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("interests", interestRepository.findAll());
            model.addAttribute("sexes", Sex.values());
            return "new";
        }

        personRepository.save(person);
        return "redirect:/persons";
    }
}
