package at.tobiasnolz.springusermanagement.presentation.web;

import at.tobiasnolz.springusermanagement.auth.Role;
import at.tobiasnolz.springusermanagement.domain.Question;
import at.tobiasnolz.springusermanagement.domain.Response;
import at.tobiasnolz.springusermanagement.domain.User;
import at.tobiasnolz.springusermanagement.domain.UserQuestion;
import at.tobiasnolz.springusermanagement.persistence.QuestionRepository;
import at.tobiasnolz.springusermanagement.persistence.UserQuestionRepository;
import at.tobiasnolz.springusermanagement.persistence.UserRepository;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.NoSuchElementException;

@Controller
public record QuestionManagement(QuestionRepository questionRepository,
                                 UserRepository userRepository,
                                 UserQuestionRepository userQuestionRepository) {

    @GetMapping()
    public String getOverview(Model model,
                              Authentication authentication) {
        var user = getUser(authentication);
        var role = user.getRole();
        model.addAttribute("user", user);

        var allQuestions = questionRepository.findAll();
        switch (role) {
            case ADMIN -> {
                model.addAttribute("questions", allQuestions);
                return "admin/question_overview";
            }
            case USER -> {
                var questionsUnresolved =
                        userQuestionRepository.findAllByUserId(user.getEmail(), LocalDate.now());
                model.addAttribute("questionsUnresolved", questionsUnresolved);
                return "user/question_overview";
            }
            default -> {
                return "access_denied";
            }
        }
    }

    @GetMapping("/questions/new")
    public String newQuestion(Model model,
                              Authentication authentication) {
        var user = getUser(authentication);
        var role = user.getRole();
        model.addAttribute("user", user);

        if (role == Role.ADMIN) {
            model.addAttribute("newQuestion", new Question());
            return "admin/question_new";
        }
        return "access_denied";
    }

    @PostMapping("/questions/new")
    public String saveQuestion(Authentication authentication,
                               @Valid @ModelAttribute("newQuestion") Question newQuestion,
                               BindingResult bindingResult,
                               Model model) {
        var user = getUser(authentication);
        var role = user.getRole();
        model.addAttribute("user", user);

        if (role != Role.ADMIN)
            return "access_denied";

        if (bindingResult.hasErrors())
            return "admin/question_new";

        questionRepository.save(newQuestion);
        return "redirect:/";
    }

    @GetMapping("/response")
    public String newResponse(Model model,
                              Authentication authentication,
                              @RequestParam("id") Long questionId) {
        var user = getUser(authentication);
        var role = user.getRole();
        model.addAttribute("user", user);

        if (role != Role.USER)
            return "access_denied";

        var question = questionRepository().findById(questionId)
                .orElseThrow(() -> new NoSuchElementException("Question not found"));

        model.addAttribute("responseOptions", Response.values());
        model.addAttribute("newResponse", new UserQuestion());
        model.addAttribute("question", question);
        return "user/question_answer";
    }

    @PostMapping("/response")
    private String saveResponse(Authentication authentication,
                                @Valid @ModelAttribute("newResponse") UserQuestion response,
                                BindingResult bindingResult,
                                Model model,
                                @RequestParam("id") Long questionId) {

        var user = getUser(authentication);
        var role = user.getRole();
        model.addAttribute("user", user);

        var question = questionRepository.findById(questionId)
                .orElseThrow(() -> new NoSuchElementException("Question not found"));

        if (role != Role.USER)
            return "access_denied";

        if (bindingResult.hasErrors()) {
            model.addAttribute("responseOptions", Response.values());
            model.addAttribute("question", question);
            return "user/question_answer";
        }

        question.addResponse(response);
        user.addResponse(response);

        userQuestionRepository.save(response);
        return "redirect:/";
    }

    private User getUser(Authentication authentication) {
        return userRepository.findById(authentication.getName())
                .orElseThrow(() -> new NoSuchElementException("No such user"));
    }
}
