package at.tobiasnolz.springusermanagement.domain;

public enum Response {
    COMPLETELY_TRUE, PARTLY_TRUE, DOES_NOT_APPLY
}
