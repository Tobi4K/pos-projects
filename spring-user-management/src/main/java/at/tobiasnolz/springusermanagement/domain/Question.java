package at.tobiasnolz.springusermanagement.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "questions")

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Question implements Serializable {

    @Id
    @GeneratedValue()
    private Long id;

    @Length(max = 20)
    @NotBlank(message = "Description may not be empty")
    private String description;

    @Length(max = 200)
    @NotBlank(message = "Text may not be empty")
    private String text;

    @NotNull(message = "Expiry date has to be set")
    private LocalDate expires;

    @OneToMany(mappedBy = "question")
    private List<UserQuestion> responses = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Question question = (Question) o;
        return id.equals(question.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Question(String description, String text, LocalDate expires) {
        this.description = description;
        this.text = text;
        this.expires = expires;
    }

    public void addResponse(UserQuestion response) {
        response.setQuestion(this);
        responses.add(response);
    }

    public int getAnswerCount() {
        return responses.size();
    }
}
