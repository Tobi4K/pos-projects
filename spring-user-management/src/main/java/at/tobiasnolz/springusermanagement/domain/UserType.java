package at.tobiasnolz.springusermanagement.domain;

public enum UserType {
    USER, ADMIN
}
