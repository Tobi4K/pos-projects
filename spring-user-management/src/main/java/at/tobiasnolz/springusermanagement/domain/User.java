package at.tobiasnolz.springusermanagement.domain;

import at.tobiasnolz.springusermanagement.auth.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.NaturalId;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "users")

@Getter
@Setter
@NoArgsConstructor
public class User implements Serializable {

    @Id
    @NaturalId
    @Email
    private String email;

    private UserType userType;

    @NotBlank
    private String password;

    private Role role;

    @OneToMany(mappedBy = "user")
    private List<UserQuestion> questions = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return email.equals(user.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email);
    }

    public User(String email, UserType userType, String password, Role role) {
        this.email = email;
        this.userType = userType;
        this.password = password;
        this.role = role;
    }

    public void addResponse(UserQuestion response) {
        response.setUser(this);
        questions.add(response);
    }
}
