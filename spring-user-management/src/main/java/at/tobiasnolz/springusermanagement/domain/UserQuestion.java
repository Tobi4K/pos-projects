package at.tobiasnolz.springusermanagement.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "user_questions",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"user_", "question"})})

@Getter
@Setter
@NoArgsConstructor

public class UserQuestion implements Serializable {

    @Id
    @GeneratedValue()
    private Long id;

    @JoinColumn(name = "user_")
    @ManyToOne(optional = false)
    private User user;

    @JoinColumn(name = "question")
    @ManyToOne(optional = false)
    private Question question;

    @NotNull(message = "Response has to be set")
    private Response response;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserQuestion that = (UserQuestion) o;
        return user.equals(that.user) && question.equals(that.question);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, question);
    }

    public UserQuestion(User user, Question question, Response response) {
        this.user = user;
        this.question = question;
        this.response = response;

        user.addResponse(this);
        question.addResponse(this);
    }
}
