package at.tobiasnolz.springusermanagement.persistence;

import at.tobiasnolz.springusermanagement.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {
}
