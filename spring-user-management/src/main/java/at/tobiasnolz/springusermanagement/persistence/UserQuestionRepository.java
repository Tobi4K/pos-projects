package at.tobiasnolz.springusermanagement.persistence;

import at.tobiasnolz.springusermanagement.domain.Question;
import at.tobiasnolz.springusermanagement.domain.UserQuestion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;

public interface UserQuestionRepository extends JpaRepository<UserQuestion, Long> {
    @Query(
            """
                    select question
                    from Question question
                    where question not in (
                        select userQuestion.question
                        from UserQuestion userQuestion
                        where userQuestion.user.email = :email
                    ) and question.expires >= :date
                    """
    )
    List<Question> findAllByUserId(String email, LocalDate date);
}
