package at.tobiasnolz.springusermanagement.persistence;

import at.tobiasnolz.springusermanagement.domain.Question;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionRepository extends JpaRepository<Question, Long> {
}
