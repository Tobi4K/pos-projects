package at.tobiasnolz.springusermanagement.persistence;

import at.tobiasnolz.springusermanagement.auth.Role;
import at.tobiasnolz.springusermanagement.domain.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public record DatabaseInitializer(QuestionRepository questionRepository,
                                  UserRepository userRepository,
                                  UserQuestionRepository userQuestionRepository,
                                  PasswordEncoder passwordEncoder) implements CommandLineRunner {
    @Override
    public void run(String... args) {
        initData();
    }

    private void initData() {
        var questions = List.of(
                new Question(
                        "Matura feel",
                        "How do you feel in regards to Corona?",
                        LocalDate.now().minusDays(10)
                ),
                new Question(
                        "Last year",
                        "Did you enjoy your last year at school?",
                        LocalDate.now().plusDays(30)
                ),
                new Question(
                        "Work life",
                        "Would you say you are prepared for the work life?",
                        LocalDate.now().plusDays(30)
                ),
                new Question(
                        "Class environment",
                        "Do you think you class has a good environment?",
                        LocalDate.now().plusDays(30)
                ),
                new Question(
                        "School",
                        "Would you recommend the school to someone else?",
                        LocalDate.now().plusDays(30)
                )
        );

        questions = questionRepository.saveAll(questions);

        var users = List.of(
                new User(
                        "t.nolz@htlstp.at",
                        UserType.ADMIN,
                        passwordEncoder.encode("pw1"),
                        Role.ADMIN
                ),
                new User(
                        "m.gail@htlstp.at",
                        UserType.USER,
                        passwordEncoder.encode("pw2"),
                        Role.USER
                ),
                new User(
                        "s.silberbauer@htlstp.at",
                        UserType.USER,
                        passwordEncoder.encode("pw3"),
                        Role.USER
                ),
                new User(
                        "a.liebhart@htlstp.at",
                        UserType.USER,
                        passwordEncoder.encode("pw4"),
                        Role.USER
                )
        );

        users = userRepository.saveAll(users);

        var responses = List.of(
                new UserQuestion(users.get(1), questions.get(0), Response.COMPLETELY_TRUE),
                new UserQuestion(users.get(1), questions.get(2), Response.DOES_NOT_APPLY),
                new UserQuestion(users.get(2), questions.get(1), Response.PARTLY_TRUE)
        );

        userQuestionRepository.saveAll(responses);
    }
}
