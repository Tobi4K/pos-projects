package at.tobiasnolz.springusermanagement.auth;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Set;
import java.util.stream.Collectors;

import static at.tobiasnolz.springusermanagement.auth.Permission.USER_READ;
import static at.tobiasnolz.springusermanagement.auth.Permission.USER_WRITE;

public enum Role {
    USER(Set.of(USER_READ)),
    ADMIN(Set.of(USER_READ, USER_WRITE));

    private final Set<Permission> permissions;

    Role(Set<Permission> permissions) {
        this.permissions = permissions;
    }

    public Set<SimpleGrantedAuthority> getAuthorities() {
        var authorities = permissions.stream()
                .map(Permission::getPermission)
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toSet());
        authorities.add(new SimpleGrantedAuthority("ROLE_" + this));
        return authorities;
    }

}