`localhost:8080/students`
* `Controller`
    * ```
      @GetMapping("/students")
      public String all(Model model) {
        model.addAttribute("xxx", 42);
        return "name-of-html-file";
      }
* Thymeleaf Template `html` -File
  ```
  th:text="${xxx}",
  th:each="",
  ```
  
---
## Formular

```
POST url

{
  "name" : "Karl",
  "age" : 42
}
```