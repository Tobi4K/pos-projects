package at.htlstp.mvc.presentation.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
public class GreetingsController {

    @GetMapping("/hello/{name}")
    public ModelAndView greet(@PathVariable String name) {
        Map<String, ?> model = Map.of("name", name);
        return new ModelAndView("greeting", model);
    }
}
