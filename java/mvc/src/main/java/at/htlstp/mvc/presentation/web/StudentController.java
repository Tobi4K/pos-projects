package at.htlstp.mvc.presentation.web;

import at.htlstp.mvc.domain.Allergy;
import at.htlstp.mvc.domain.Student;
import at.htlstp.mvc.persistence.StudentRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/students")
public record StudentController(StudentRepository studentRepository) {

    @ModelAttribute
    private Object getfoo() {
        return "ASD";
    }

    @GetMapping("/{id}")
    public String detail(@PathVariable Long id, Model model) {
        var student = studentRepository
                .findById(id)
                .orElseThrow();

        model.addAttribute("student", student);
        return "/students/detail";
    }

    @GetMapping
    public String all(Model model) {
        model.addAttribute("students", studentRepository.findAll());
        return "/students/all";
    }

    @GetMapping("add")
    public String newStudentForm(Model model) {
        model.addAttribute("student", new Student());
        model.addAttribute("allergies", Allergy.values());
        return "/students/new";
    }

    @PostMapping
    public String addNewStudent(
            @Valid Student student,
            BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "/students/new";
        }
        studentRepository.save(student);
        return "redirect:/students";    // GET /students
    }
}
