package at.htlstp.mvc.persistence;

import at.htlstp.mvc.domain.Student;
import org.springframework.data.repository.CrudRepository;

import javax.validation.constraints.Positive;
import java.util.List;

//@RestResource(path="api/students", rel="students")
public interface StudentRepository extends CrudRepository<Student, Long> {

    List<Student> findAllByNumberIsLessThan(@Positive Integer number);
}