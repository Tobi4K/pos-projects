package at.htlstp.mvc.domain;


public enum Allergy {
    LACTOSE, NUTS, SANITY;
}
