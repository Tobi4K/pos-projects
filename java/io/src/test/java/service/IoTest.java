package service;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.*;
import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;

public class IoTest {

    @Nested
    class ReadingTextFiles {

        @Test
        @SneakyThrows
        void works_as_pom_sibling() {
            var path = Path.of("input-bad.txt");

            var lines = Files.readAllLines(path);

            assertThat(lines)
                    .hasSize(3);
        }

        @Test
        @SneakyThrows
        void works_from_resources() {
            var path = Path.of(getClass().getResource("/input.txt").toURI());

            var lines = Files.readAllLines(path);

            assertThat(lines)
                    .hasSize(3);
        }

        @Test
        @SneakyThrows
        void with_any_length() {
            var path = Path.of(getClass().getResource("/input.txt").toURI());

            try (var lines = Files.lines(path)) {
                assertThat(lines)
                        .hasSize(3);
            }
        }

        @Test
        @SneakyThrows
        void with_readers() {
            try (
                    var stream = getClass().getResourceAsStream("/input.txt");
                    var reader = new BufferedReader(new InputStreamReader(stream))
            ) {
                assertThat(reader.lines())
                        .hasSize(3);
            }
        }
    }

    @Nested
    class WritingTextFiles {

        @Test
        @SneakyThrows
        void with_Files() {
            var path = Path.of("output.txt");

            Files.writeString(path, "Ja\n");
            Files.writeString(path, "Nein\n", StandardOpenOption.APPEND);
            Files.writeString(path, "Vielleicht\n", StandardOpenOption.APPEND);

            assertThat(Files.readAllLines(path))
                    .hasSize(3);
        }

        @Test
        @SneakyThrows
        void with_Streams() {
            var path = Path.of("output.txt");

            try (var writer = new PrintWriter(new FileWriter("output.txt"))) {
                writer.println("1");
                writer.println("2");
                writer.println("3");
            }

            assertThat(Files.readAllLines(path))
                    .hasSize(3);
        }
    }

    @Nested
    class WritingBinaryFiles {

        @Test
        @SneakyThrows
        void with_raf() {
            try (var file = new RandomAccessFile("output.bin", "rw")) {
                file.writeLong(42L);
                file.writeUTF("Hallo");
                file.writeDouble(3.1415);
            }

            assertThat(new File("output.bin").length())
                    .isEqualTo(23);
        }

        @Test
        @SneakyThrows
        void with_streams() {
            try (var stream = new DataOutputStream(new FileOutputStream("output.bin"))) {
                stream.writeLong(42L);
                stream.writeUTF("Hallo");
                stream.writeDouble(3.1415);
            }

            assertThat(new File("output.bin").length())
                    .isEqualTo(23);
        }
    }

    @Nested
    class ReadingBinaryFiles {

        @Test
        @SneakyThrows
        void with_raf() {
            var contents = new ArrayList<Object>();

            try (var file = new RandomAccessFile("output.bin", "r")) {
                contents.add(file.readLong());
                contents.add(file.readUTF());
                contents.add(file.readDouble());
            }

            assertThat(contents)
                    .containsExactly(42L, "Hallo", 3.1415);
        }

        @Test
        @SneakyThrows
        void with_streams() {
            var contents = new ArrayList<Object>();

            try (var stream = new DataInputStream(new FileInputStream("output.bin"))) {
                contents.add(stream.readLong());
                contents.add(stream.readUTF());
                contents.add(stream.readDouble());
            }

            assertThat(contents)
                    .containsExactly(42L, "Hallo", 3.1415);
        }
    }
}
