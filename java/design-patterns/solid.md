## SOLID

#### SRP - Single Responsibility
- jedes Ding soll sich nur aus einem Grund ändern können
- Input/Output auslagern

#### OCP - Open Closed
- Klassen sollten sich nicht verändern
- Motivation für viele Design Pattern

#### LSP - Liskov Substitution
- Interfaces vollständig implementieren
- ~~throw new UnsupportedOperationException~~

#### ISP - Interface Segregation
- SRP gilt auch für Interfaces

#### DIP - Dependency Inversion
- alle Compiletypen Interfaces(außer domain)
- Dependencies über Konstruktor setzen