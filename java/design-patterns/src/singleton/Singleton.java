package singleton;

import java.util.List;

public class Singleton {

    private final String jdbcUrl;
    private static Singleton instance = new Singleton("asd");

    private Singleton(String jdbcUrl) {
        this.jdbcUrl = jdbcUrl;
    }

    public static Singleton getInstance() {
        if (instance == null)
            instance = new Singleton("asd");
        return instance;
    }

    public List<String> findAll() {
        return List.of(jdbcUrl);
    }
}
