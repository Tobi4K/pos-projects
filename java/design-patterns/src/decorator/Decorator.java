package decorator;

public abstract class Decorator implements Service{

    private Service service;

    @Override
    public String method() {
        return service.method();
    }
}
