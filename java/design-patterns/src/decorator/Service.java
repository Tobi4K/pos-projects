package decorator;

public interface Service {
    String method();
}
