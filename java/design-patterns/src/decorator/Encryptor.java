package decorator;

public class Encryptor extends Decorator implements Service{

    public Encryptor(ServiceImpl service) {
        super();
    }

    @Override
    public String method() {
        // Decorator.method()
        return super.method().toUpperCase();
    }
}
