package decorator;

public class App {

    public static void main(String[] args) {
        Service service = new Encryptor(new ServiceImpl());
        System.out.println(service.method());
    }
}
