package builder;

public interface PizzaBuilder {

    Pizza build();
}
