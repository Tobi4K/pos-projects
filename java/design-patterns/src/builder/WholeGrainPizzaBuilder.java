package builder;

public class WholeGrainPizzaBuilder implements PizzaBuilder{

    private boolean cheese;
    private boolean tomatoes;
    private boolean tuna;
    private boolean onions;
    private boolean garlic;
    private boolean chili;
    private boolean kebab;
    private boolean gorgonzola;
    private boolean rocket;
    private boolean salami;
    private double size;

    public WholeGrainPizzaBuilder setCheese(boolean cheese) {
        this.cheese = cheese;
        return this;
    }

    public WholeGrainPizzaBuilder setTomatoes(boolean tomatoes) {
        this.tomatoes = tomatoes;
        return this;
    }

    public WholeGrainPizzaBuilder setTuna(boolean tuna) {
        this.tuna = tuna;
        return this;
    }

    public WholeGrainPizzaBuilder setOnions(boolean onions) {
        this.onions = onions;
        return this;
    }

    public WholeGrainPizzaBuilder setGarlic(boolean garlic) {
        this.garlic = garlic;
        return this;
    }

    public WholeGrainPizzaBuilder setChili(boolean chili) {
        this.chili = chili;
        return this;
    }

    public WholeGrainPizzaBuilder setKebab(boolean kebab) {
        this.kebab = kebab;
        return this;
    }

    public WholeGrainPizzaBuilder setGorgonzola(boolean gorgonzola) {
        this.gorgonzola = gorgonzola;
        return this;
    }

    public WholeGrainPizzaBuilder setRocket(boolean rocket) {
        this.rocket = rocket;
        return this;
    }

    public WholeGrainPizzaBuilder setSalami(boolean salami) {
        this.salami = salami;
        return this;
    }

    public WholeGrainPizzaBuilder setSize(double size) {
        this.size = size;
        return this;
    }

    public Pizza build() {
        return new Pizza(cheese, tomatoes, tuna, onions, garlic, chili, kebab, gorgonzola, rocket, salami, size);
    }
}