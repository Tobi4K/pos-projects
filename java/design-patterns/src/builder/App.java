package builder;

public class App {

    public static void main(String[] args) {
        //new Pizza();
        var pizza = new WholeGrainPizzaBuilder()
                .setCheese(true)
                .setOnions(true)
                .setRocket(true)
                .setSize(20)
                .build();
    }
}
