package builder;

//@Builder
public class Pizza {

    private boolean cheese;
    private boolean tomatoes;
    private boolean tuna;
    private boolean onions;
    private boolean garlic;
    private boolean chili;
    private boolean kebab;
    private boolean gorgonzola;
    private boolean rocket;
    private boolean salami;
    private double size;

    public Pizza(boolean cheese, boolean tomatoes, boolean tuna, boolean onions, boolean garlic, boolean chili, boolean kebab, boolean gorgonzola, boolean rocket, boolean salami, double size) {
        this.cheese = cheese;
        this.tomatoes = tomatoes;
        this.tuna = tuna;
        this.onions = onions;
        this.garlic = garlic;
        this.chili = chili;
        this.kebab = kebab;
        this.gorgonzola = gorgonzola;
        this.rocket = rocket;
        this.salami = salami;
        this.size = size;
    }
}
