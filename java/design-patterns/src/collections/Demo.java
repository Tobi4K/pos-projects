package collections;

import java.util.*;
import java.util.stream.Stream;

public class Demo {

    public static void main(String[] args) {
        PriorityQueue<Person> persons = new PriorityQueue<>();
        persons.add(new Person("A", 10));
        persons.add(new Person("B", 5));
        persons.add(new Person("C", 15));
        while(!persons.isEmpty()) {
            var currentPerson = persons.remove();
            System.out.println(currentPerson);
        }

        Stream
                .of()
                .map(Object::toString)
                .max(Comparator.comparing(String::length));

    }
}

record Person(String name, int age) implements Comparable<Person>{

    @Override
    public int compareTo(Person other) {
        return Comparator
                .comparingInt(Person::age)
                .thenComparing(Person::name)
                .compare(this,other);
    }
}