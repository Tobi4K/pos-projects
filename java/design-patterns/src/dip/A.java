package dip;

public class A {

    private I i;

    public A(I i) {
        this.i = i;
    }

    public void bar() {
        i.foo();
    }
}

interface I {

    void foo();
}

class B implements I{

    public void foo() {}
}