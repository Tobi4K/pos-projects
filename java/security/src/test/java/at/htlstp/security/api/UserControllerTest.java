package at.htlstp.security.api;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @WithMockUser
    void finding_by_user_name_works() throws Exception {
        mockMvc
                .perform(get("/api/users/1"))
                .andExpect(status().isOk())
                .andExpect(content().json(
                        """
                                {
                                  "id": 1,
                                  "username": "user1",
                                  "data": null
                                }
                                """
                ));
    }

    @Test
    @WithMockUser("user1")
    void finding_self_works() throws Exception {
        mockMvc
                .perform(get("/api/users"))
                .andExpect(status().isOk())
                .andExpect(content().json(
                        """
                                {
                                  "id": 1,
                                  "username": "user1",
                                  "data": null
                                }
                                """
                ));
    }

    @Test
    @DirtiesContext
    @WithUserDetails("user1")
    void saving_fails_for_normal_user() throws Exception {
        var json = """
                {
                  "username": "user",
                  "password": "pw"
                }
                """;
        var request = post("/api/users")
                .contentType(APPLICATION_JSON)
                .content(json);

        mockMvc.perform(request)
                .andExpect(status().isForbidden());
    }

    @Test
    @WithUserDetails("admin")
    void saving_works_for_admin() throws Exception {
        var json = """
                {
                  "username": "user",
                  "password": "pw"
                }
                """;
        var request = post("/api/users")
                .contentType(APPLICATION_JSON)
                .content(json);

        mockMvc.perform(request)
                .andExpect(status().isOk());
    }

}
