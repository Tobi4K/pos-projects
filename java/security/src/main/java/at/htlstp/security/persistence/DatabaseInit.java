package at.htlstp.security.persistence;

import at.htlstp.security.auth.Roles;
import at.htlstp.security.domain.User;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public record DatabaseInit(UserRepository userRepository,
                           PasswordEncoder passwordEncoder) implements CommandLineRunner {

    @Override
    public void run(String... args) {
        if (userRepository.count() > 0)
            return;
        userRepository.saveAll(List.of(
                new User(null, "user1", passwordEncoder.encode("pw1"), null, Roles.USER),
                new User(null, "user2", passwordEncoder.encode("pw2"), 1, Roles.USER),
                new User(null, "admin", passwordEncoder.encode("pw3"), 42, Roles.ADMIN)
        ));
    }
}
