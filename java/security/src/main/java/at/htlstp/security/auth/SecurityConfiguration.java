package at.htlstp.security.auth;

import lombok.RequiredArgsConstructor;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

import static at.htlstp.security.auth.Permissions.USER_WRITE;
import static at.htlstp.security.auth.Roles.ADMIN;
import static at.htlstp.security.auth.Roles.USER;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.PUT;

@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final PasswordEncoder passwordEncoder;
    private final UserDetailsService userDetailsService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        basic(http);
    }

    private void basic(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/home", "/css/**", "/static/**", "/webjars/**").permitAll()
                .antMatchers(POST, "/api/users/**").hasRole(ADMIN.name())
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login").permitAll()
                .defaultSuccessUrl("/greeting", true)
                .and()
                .rememberMe()
                .and()
                .logout()
                .logoutUrl("/logout")
                .clearAuthentication(true)      // default
                .invalidateHttpSession(true)    //default
                .logoutSuccessUrl("/login");
    }

    private void withRemembering(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/", "/home").permitAll()
                .antMatchers(PUT, "/api/users/**").hasAuthority(USER_WRITE.getPermission())
                .anyRequest().authenticated()
                .and()
                .httpBasic();
    }

    private void free(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .anyRequest().permitAll();
    }

    private void standard(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .anyRequest().authenticated()
                .and()
                .formLogin();
    }

    private void whitelisted(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/", "/home").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        //useInMemoryUserDetailsManager(auth);
        auth.userDetailsService(userDetailsService);
    }

    private void useInMemoryUserDetailsManager(AuthenticationManagerBuilder auth) throws Exception {
        var configurer = auth.inMemoryAuthentication();
        configurer.withUser(User.withUsername("user1")
                .password(passwordEncoder.encode("pw1"))
                .roles(USER.name())
        ).withUser(User.withUsername("user2")
                .password(passwordEncoder.encode("pw2"))
                .authorities(USER.getAuthorities())
        ).withUser(User.withUsername("admin")
                .password(passwordEncoder.encode("pw3"))
                .roles(ADMIN.name())
        );
    }

}
