package at.htlstp.security.auth;

import at.htlstp.security.persistence.UserRepository;
import org.springframework.security.core.userdetails.*;
import org.springframework.stereotype.Service;

@Service
public record RepositoryUserDetailsService(UserRepository userRepository) implements UserDetailsService {

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        var user = userRepository
                .findByUsername(username)
                .orElseThrow();
        return User
                .builder()
                .username(user.getUsername())
                .password(user.getPassword())
                .roles(user.getRoles().name())
                .build();
    }
}
