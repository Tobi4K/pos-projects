package at.htlstp.security.presentation.api;

import at.htlstp.security.domain.User;
import at.htlstp.security.persistence.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("api/users")
@RequiredArgsConstructor
public class UserController {

    private final UserRepository userRepository;

    @GetMapping
    User self(Principal principal) {
        var name = principal.getName();
        return userRepository
                .findByUsername(name)
                .orElseThrow();
    }

    @GetMapping("/{id}")
    User get(@PathVariable long id) {
        return userRepository
                .findById(id)
                .orElseThrow();
    }

    @PostMapping
    User save(@RequestBody User user) {
        return userRepository.save(user);
    }

    @PutMapping("/{id}")
    User update(@PathVariable long id, @RequestBody User user) {
        return userRepository.save(user);
    }
}
