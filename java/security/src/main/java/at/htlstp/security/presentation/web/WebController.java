package at.htlstp.security.presentation.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

@Controller
public class WebController {

    @GetMapping({"/home"})
    public String showHome() {
        return "home";
    }

    @GetMapping("greeting")
    public String showGreeting(Model model, Principal principal) {
        model.addAttribute("username", principal.getName());
        return "greeting";
    }

    @GetMapping("login")
    public String showLogin() {
        return "login";
    }
}
