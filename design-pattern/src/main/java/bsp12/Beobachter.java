package bsp12;

import java.time.LocalDateTime;

public interface Beobachter {
    void notify(Konto konto, double betrag, LocalDateTime timestamp);
}
