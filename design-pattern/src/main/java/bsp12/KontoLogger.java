package bsp12;

import lombok.SneakyThrows;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.logging.Logger;

public class KontoLogger implements Beobachter{

    private final Path path;
    private final Logger logger;

    public KontoLogger(Path path, Logger logger) {
        this.path = path;
        this.logger = logger;
    }

    @Override
    public void notify(Konto konto, double betrag, LocalDateTime timestamp) {
        logToConsole(konto, betrag, timestamp);
        logToFile(konto, betrag, timestamp);
    }

    @SneakyThrows
    private void logToFile(Konto konto, double betrag, LocalDateTime timestamp) {
        Files.writeString(path, getLogMessage(konto, betrag, timestamp) + "\n", StandardOpenOption.APPEND);
    }

    private void logToConsole(Konto konto, double betrag, LocalDateTime timestamp) {
        logger.info(getLogMessage(konto, betrag, timestamp));
    }

    private String getLogMessage(Konto konto, double betrag, LocalDateTime timestamp) {
        return timestamp + ": " + konto.getKontonummer() + " versuchte " + betrag + "€ abzuheben";
    }
}
