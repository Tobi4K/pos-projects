package bsp12;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Konto {
    private final long kontonummer;
    private double kontostand;
    private final double limit;

    private final List<Beobachter> beobachterListe = new ArrayList<>();

    public Konto(long kontonummer, double limit) {
        this.kontonummer = kontonummer;
        this.limit = limit;
    }

    public void einzahlen(double betrag) {
        if (betrag > 0) {
            kontostand += betrag;
        }
    }

    public boolean auszahlen(double betrag) {
        if (betrag > 0 && kontostand - betrag >= limit) {
            kontostand -= betrag;
            return true;
        }
        var now = LocalDateTime.now();
        beobachterListe.forEach(beobachter -> beobachter.notify(this, betrag, now));
        return false;
    }

    public double getKontostand() {
        return kontostand;
    }

    public long getKontonummer() {
        return kontonummer;
    }

    public void subscribe(Beobachter beobachter) {
        beobachterListe.add(beobachter);
    }

    public void unsubscribe(Beobachter beobachter) {
        beobachterListe.remove(beobachter);
    }
}
