package bsp12;

import lombok.SneakyThrows;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.logging.Logger;

public class KontoSimulation {
    @SneakyThrows
    public static void main(String[] args) {

        var logger = Logger.getLogger(KontoSimulation.class.getName());
        var fileName = Path.of("log.txt");
        Files.createFile(fileName);
        var kontoLogger = new KontoLogger(fileName, logger);

        var konto = new Konto(123456, -1000);
        konto.subscribe(kontoLogger);

        konto.einzahlen(1000);
        System.out.println(konto.getKontostand());
        konto.auszahlen(500);
        System.out.println(konto.getKontostand());
        System.out.println(konto.auszahlen(99999));
        System.out.println(konto.auszahlen(10000));
    }
}
