package patterns.proxy.util;

import java.time.LocalDateTime;
import java.util.logging.Logger;

public class ServiceProxy implements Service {

    private final Service wrapped = new ServiceImpl();
    @Override
    public void foo() {
        var logger = Logger.getLogger(wrapped.getClass().getSimpleName());
        logger.info("Service execution started at " + LocalDateTime.now());
        wrapped.foo();
        logger.info("Service execution finished at " + LocalDateTime.now());
    }
}
