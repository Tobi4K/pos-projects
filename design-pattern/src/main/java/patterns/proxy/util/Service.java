package patterns.proxy.util;

public interface Service {
    void foo();
}
