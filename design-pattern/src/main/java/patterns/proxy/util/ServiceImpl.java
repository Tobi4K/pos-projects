package patterns.proxy.util;

public class ServiceImpl implements Service {

    @Override
    public void foo() {
        System.out.println("Executed foo");
    }
}
