package patterns.proxy.app;

import patterns.proxy.util.ServiceProxy;

public class Client {
    public static void main(String[] args) {
        var service = new ServiceProxy();
        service.foo();
    }
}
