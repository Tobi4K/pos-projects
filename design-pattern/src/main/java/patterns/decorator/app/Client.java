package patterns.decorator.app;

import patterns.decorator.util.*;

public class Client {
    public static void main(String[] args) {
        Coffee coffee = new BasicCoffee();
        System.out.println("Ingredients: " + coffee.getIngredients() + " - Price: " + coffee.getPrice());

        coffee = new CoffeeWithMilk(coffee);
        System.out.println("Ingredients: " + coffee.getIngredients() + " - Price: " + coffee.getPrice());

        coffee = new CoffeeWithSugar(coffee);
        System.out.println("Ingredients: " + coffee.getIngredients() + " - Price: " + coffee.getPrice());

        coffee = new CoffeeWithSyrup(coffee);
        System.out.println("Ingredients: " + coffee.getIngredients() + " - Price: " + coffee.getPrice());
    }
}
