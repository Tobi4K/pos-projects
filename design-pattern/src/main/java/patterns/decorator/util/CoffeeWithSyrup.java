package patterns.decorator.util;

import java.util.List;

public class CoffeeWithSyrup extends UpgradedCoffee {

    public CoffeeWithSyrup(Coffee wrappee) {
        super(wrappee);
    }

    @Override
    public List<Ingredient> getIngredients() {
        var ingredients = super.getIngredients();
        ingredients.add(Ingredient.SYRUP);
        return ingredients;
    }

    @Override
    public double getPrice() {
        return super.getPrice() + 0.75;
    }
}
