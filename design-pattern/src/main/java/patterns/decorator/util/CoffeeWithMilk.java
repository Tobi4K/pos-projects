package patterns.decorator.util;

import java.util.List;

public class CoffeeWithMilk extends UpgradedCoffee {

    public CoffeeWithMilk(Coffee wrappee) {
        super(wrappee);
    }

    @Override
    public List<Ingredient> getIngredients() {
        var ingredients = super.getIngredients();
        ingredients.add(Ingredient.MILK);
        return ingredients;
    }

    @Override
    public double getPrice() {
        return super.getPrice() + 0.5;
    }
}
