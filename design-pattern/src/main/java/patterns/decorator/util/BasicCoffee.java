package patterns.decorator.util;

import java.util.LinkedList;
import java.util.List;

public class BasicCoffee implements Coffee {

    @Override
    public List<Ingredient> getIngredients() {
        var ingredients = new LinkedList<Ingredient>();
        ingredients.add(Ingredient.WATER);
        return ingredients;
    }

    @Override
    public double getPrice() {
        return 1;
    }
}
