package patterns.decorator.util;

import java.util.List;

public interface Coffee {
    List<Ingredient> getIngredients();
    double getPrice();
}
