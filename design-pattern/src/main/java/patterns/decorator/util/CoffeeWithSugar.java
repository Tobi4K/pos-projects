package patterns.decorator.util;

import java.util.List;

public class CoffeeWithSugar extends UpgradedCoffee {

    public CoffeeWithSugar(Coffee wrappee) {
        super(wrappee);
    }

    @Override
    public List<Ingredient> getIngredients() {
        var ingredients = super.getIngredients();
        ingredients.add(Ingredient.SUGAR);
        return ingredients;
    }

    @Override
    public double getPrice() {
        return super.getPrice() + 1.0;
    }
}
