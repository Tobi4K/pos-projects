package patterns.decorator.util;

public enum Ingredient {
    WATER, MILK, SUGAR, SYRUP
}
