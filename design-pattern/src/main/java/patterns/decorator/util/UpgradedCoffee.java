package patterns.decorator.util;

import java.util.List;

public abstract class UpgradedCoffee implements Coffee {

    private final Coffee wrappee;

    protected UpgradedCoffee(Coffee wrappee) {
        this.wrappee = wrappee;
    }

    @Override
    public List<Ingredient> getIngredients() {
        return wrappee.getIngredients();
    }

    @Override
    public double getPrice() {
        return wrappee.getPrice();
    }
}
