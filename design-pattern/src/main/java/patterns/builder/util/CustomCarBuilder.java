package patterns.builder.util;

import java.time.LocalDate;

public class CustomCarBuilder {
    private String brand;
    private String model;
    private int ps;
    private LocalDate productionDate;
    private int releaseYear;
    private String color;
    private double price;
    private int capacity;

    public CustomCarBuilder setBrand(String brand) {
        this.brand = brand;
        return this;
    }

    public CustomCarBuilder setModel(String model) {
        this.model = model;
        return this;
    }

    public CustomCarBuilder setPs(int ps) {
        this.ps = ps;
        return this;
    }

    public CustomCarBuilder setProductionDate(LocalDate productionDate) {
        this.productionDate = productionDate;
        return this;
    }

    public CustomCarBuilder setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
        return this;
    }

    public CustomCarBuilder setColor(String color) {
        this.color = color;
        return this;
    }

    public CustomCarBuilder setPrice(double price) {
        this.price = price;
        return this;
    }

    public CustomCarBuilder setCapacity(int capacity) {
        this.capacity = capacity;
        return this;
    }

    public Car createCar() {
        return new Car(brand, model, ps, productionDate, releaseYear, color, price, capacity);
    }
}