package patterns.builder.util;

import lombok.Builder;

import java.time.LocalDate;


public class Car {

    private String brand;
    private String model;
    private int ps;
    private LocalDate productionDate;
    private int releaseYear;
    private String color;
    private double price;
    private int capacity;

    @Builder
    public Car(String brand, String model, int ps, LocalDate productionDate, int releaseYear, String color, double price, int capacity) {
        this.brand = brand;
        this.model = model;
        this.ps = ps;
        this.productionDate = productionDate;
        this.releaseYear = releaseYear;
        this.color = color;
        this.price = price;
        this.capacity = capacity;
    }

    @Override
    public String toString() {
        return "Car{" +
               "brand='" + brand + '\'' +
               ", model='" + model + '\'' +
               ", ps=" + ps +
               ", productionDate=" + productionDate +
               ", releaseYear=" + releaseYear +
               ", color='" + color + '\'' +
               ", price=" + price +
               ", capacity=" + capacity +
               '}';
    }
}
