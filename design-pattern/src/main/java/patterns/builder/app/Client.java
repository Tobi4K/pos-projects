package patterns.builder.app;

import patterns.builder.util.Car;
import patterns.builder.util.CustomCarBuilder;

public class Client {
    public static void main(String[] args) {

        /* Lombok Builder */
        var builder = Car.builder();
        builder.brand("Tesla")
                .model("Model 3")
                .color("White")
                .ps(500)
                .price(50000);

        var car = builder.build();
        System.out.println(car);

        /* IntelliJ Builder */
        var customBuilder = new CustomCarBuilder();
        customBuilder.setBrand("Tesla")
                .setModel("Model 3")
                .setColor("White")
                .setPs(500)
                .setPrice(50000);

        car = customBuilder.createCar();
        System.out.println(car);

    }
}
