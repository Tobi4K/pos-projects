package patterns.adapter.util;

public record NewServiceAdapter(OldService oldService) implements NewService {

    @Override
    public void baa() {
        System.out.print("Adapter redirects request: ");
        oldService.foo();
    }
}
