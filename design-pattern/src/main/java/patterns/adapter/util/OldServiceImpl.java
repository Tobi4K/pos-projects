package patterns.adapter.util;

public class OldServiceImpl implements OldService {

    private int counter;

    @Override
    public void foo() {
        System.out.println("Foo executed " + ++counter + " times");
        // monkaS bei der Syntax -100 punkte
    }
}
