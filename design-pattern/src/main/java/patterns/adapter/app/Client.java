package patterns.adapter.app;

import patterns.adapter.util.NewServiceAdapter;
import patterns.adapter.util.OldService;
import patterns.adapter.util.OldServiceImpl;

import java.util.stream.IntStream;

public class Client {

    private OldService oldService;

    public static void main(String[] args) {

        var existingService = new OldServiceImpl();
        var service = new NewServiceAdapter(existingService);

        IntStream.range(0, 10)
                .forEach(i -> service.baa());

    }
}
