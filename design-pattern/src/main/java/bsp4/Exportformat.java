package bsp4;

public interface Exportformat {
    void export(Document d);
}

class XMLFormat implements Exportformat {
    public void export(Document d) {
        System.out.println("Export im XML-Format");
        // Export von d im XML-Format
    }
}

class PDFFormat implements Exportformat {
    public void export(Document d) {
        System.out.println("Export im PDF-Format");
        // Export von d im PDF-Format
    }
}

class Main {
    public static void main(String[] args) {
        Document doc = new Document();
        Exporter ex = new Exporter(new PDFFormat());
        ex.export(doc);
        ex.doSomething(new XMLFormat());
        ex.export(doc);
    }
}

class Document {
}

class Exporter {
    private Exportformat format;

    public Exporter(Exportformat format) {
        this.format = format;
    }

    public void doSomething(Exportformat format) {
        this.format = format;
    }

    public void export(Document d) {
        format.export(d);
    }
}