package bsp9;

import java.util.List;

public class Navigator {

    private RouteStrategy routeStrategy;

    public Navigator() {
    }

    public void setRouteStrategy(RouteStrategy routeStrategy) {
        this.routeStrategy = routeStrategy;
    }

    public Route calcRoute(List<Route> routes) {
        return routeStrategy.calcRoute(routes);
    }
}

