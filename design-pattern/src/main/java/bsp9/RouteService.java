package bsp9;

import java.util.List;

public final class RouteService {

    private RouteService() {
    }

    public static List<Route> getWIEN_FLORENZ() {
        return List.of(
                new Route("Route Wien-Floren über Graz", 493, 852),
                new Route("Route Wien-Floren über Leoben", 505, 828),
                new Route("demo other", 999, 999)
        );
    }
}
