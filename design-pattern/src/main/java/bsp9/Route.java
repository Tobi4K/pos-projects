package bsp9;

public class Route {

    private String description;
    private int duration;
    private int distance;

    public Route(String description, int duration, int distance) {
        this.description = description;
        this.duration = duration;
        this.distance = distance;
    }

    public String getDescription() {
        return description;
    }

    public int getDuration() {
        return duration;
    }

    public int getDistance() {
        return distance;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return description + ": " + distance + " km, " + duration + " min";
    }
}
