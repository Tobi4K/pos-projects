package bsp9;

public class App {
    public static void main(String[] args) {
        var routes = RouteService.getWIEN_FLORENZ();
        var nav = new Navigator();

        nav.setRouteStrategy(new FastestRoute());
        System.out.println("Fastest:  " + nav.calcRoute(routes));

        nav.setRouteStrategy(new ShortestRoute());
        System.out.println("Shortest: " + nav.calcRoute(routes));
    }
}
