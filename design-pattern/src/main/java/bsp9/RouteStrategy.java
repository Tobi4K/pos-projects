package bsp9;

import java.util.Comparator;
import java.util.List;

public interface RouteStrategy {
    Route calcRoute(List<Route> routes);
}

class FastestRoute implements RouteStrategy {

    @Override
    public Route calcRoute(List<Route> routes) {
        var route = routes
                .stream()
                .min(Comparator.comparing(Route::getDuration));

        return route.orElseThrow(() -> new IllegalArgumentException("Routes invalid"));
    }
}

class ShortestRoute implements RouteStrategy {
    @Override
    public Route calcRoute(List<Route> routes) {
        var route = routes
                .stream()
                .min(Comparator.comparing(Route::getDistance));

        return route.orElseThrow(() -> new IllegalArgumentException("Routes invalid"));
    }

}