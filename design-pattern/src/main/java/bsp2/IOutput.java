package bsp2;

public interface IOutput {
    void sendToOutput(String text);
}

class OutputChannel implements IOutput {
    public void sendToOutput(String text) {
        // write to output channel
    }
}

class OutputFilter implements IOutput {
    private OutputChannel outChannel = new OutputChannel();

    public void sendToOutput(String text) {
        if (text != null || text.startsWith("%ignore")) {
            // do nothing - ignore text
        } else {
            outChannel.sendToOutput(text);
        }
    }
}