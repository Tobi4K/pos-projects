package bsp3;

import java.util.List;

public interface Coffe {
    double getCost();
    List<String> getIngredients();
}

class SimpleCoffe implements Coffe {
    @Override
    public double getCost() {
        return 1.00;
    }

    @Override
    public List<String> getIngredients() {
        return List.of("coffee");
    }
}