package bsp3;

import java.util.ArrayList;
import java.util.List;

public abstract class CoffeeAdditional implements Coffe {

    private Coffe wrappee;

    public CoffeeAdditional(Coffe wrappee) {
        this.wrappee = wrappee;
    }

    public double getCost() {
        return wrappee.getCost();
    }

    public List<String> getIngredients() {
        return wrappee.getIngredients();
    }
}

class WithMilk extends CoffeeAdditional {

    public WithMilk(Coffe wrappee) {
        super(wrappee);
    }

    @Override
    public double getCost() {
        return super.getCost() + 0.5;
    }

    @Override
    public List<String> getIngredients() {
        var ingredients = super.getIngredients();
        ingredients.add("Milk");
        return ingredients;
    }
}

class WithSprinkles extends CoffeeAdditional {

    public WithSprinkles(Coffe wrappee) {
        super(wrappee);
    }

    @Override
    public double getCost() {
        return super.getCost() + 0.2;
    }

    @Override
    public List<String> getIngredients() {
        var ingredients = super.getIngredients();
        ingredients.add("Sprinkles");
        return ingredients;
    }
}