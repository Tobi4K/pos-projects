package bsp11;

public interface IKreis<T> {
    /**
     * Stellt den internen Zeiger im Kreis auf das erste Kind.
     *
     * @throws IllegalStateException, wenn der Kreis leer ist
     */
    void init();

    /**
     * Fügt ein neues Element an der Endeposition ein
     *
     * @param t das einzufügende Element
     * @return true bei Erfolg, false bei einem Fehler
     */
    boolean add(T t);

    /**
     * Setzt den internen Zeiger um n Positionen weiter
     *
     * @param n Anzahl der Positionen
     * @throws IllegalStateException, wenn der Kreis leer ist
     */
    void count(int n);

    /**
     * Läscht und liefert das Element im Kreis, auf das der
     * interne Zeiger aktuell verweist.*@return das gel ̈oschte Element
     *
     * @throws IllegalStateException, wenn der Kreis leer ist
     */
    T deleteAkt();

    /**
     * Liefert die aktuelle Gr ̈oße des Kreises
     *
     * @return die aktuelle Gr ̈oße
     */
    int size();
}
