package bsp11;

import java.util.LinkedList;
import java.util.List;

public class Kreis<T> implements IKreis<T> {

    private List<T> adaptee = new LinkedList<>();
    private int pointer;

    public Kreis() {
    }

    @Override
    public void init() {
        checkEmpty();
        pointer = size() - 1;
    }

    @Override
    public boolean add(T t) {
        return adaptee.add(t);
    }

    @Override
    public void count(int n) {
        checkEmpty();
        pointer += n;
        if (size() == 0)
            pointer = 0;
        else
            pointer %= size();
    }

    @Override
    public T deleteAkt() {
        checkEmpty();
        var element = adaptee.remove(pointer);
        pointer--;
        if (size() == 0)
            pointer = 0;
        else
            pointer %= size();
        return element;
    }

    @Override
    public int size() {
        return adaptee.size();
    }

    private void checkEmpty() {
        if (adaptee.isEmpty())
            throw new IllegalStateException("Kreis darf nicht leer sein");
    }
}
