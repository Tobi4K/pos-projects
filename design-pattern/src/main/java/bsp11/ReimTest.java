package bsp11;

import java.util.Arrays;

public class ReimTest {
    public static void main(String[] args) {

        String[] kids = {"Adam", "Julia", "Franz", "Christian", "Katharina", "Eva"};
        final Kreis<String> kreis = new Kreis<>();

        Arrays.stream(kids).forEach(kreis::add);
        int silben = 3;

        kreis.init();
        while (kreis.size() > 1) {
            kreis.count(silben);
            System.out.println(kreis.deleteAkt());
        }
        System.out.println("Sieger: " + kreis.deleteAkt());
    }
}
