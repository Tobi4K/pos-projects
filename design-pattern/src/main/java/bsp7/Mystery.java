package bsp7;

import java.util.ArrayList;
import java.util.List;

public class Mystery implements Publisher {
    private List<Subscriber> other = new ArrayList<Subscriber>();

    @Override
    public void addOther(Subscriber o) {
        other.add(o);
    }

    @Override
    public void removeOther(Subscriber o) {
        other.remove(o);
    }

    @Override
    public void notifyOther() {
        other.forEach(o -> o.call(this));
    }

}
