package bsp7;

public class Other implements Subscriber {
    public void call(Publisher publisher) {
        System.out.println(this + " was called");
    }
}
