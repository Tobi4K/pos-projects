package bsp7;

public interface Publisher {
    void addOther(Subscriber o);

    void removeOther(Subscriber o);

    void notifyOther();
}
