package bsp7;

public interface Subscriber {
    void call(Publisher publisher);
}
