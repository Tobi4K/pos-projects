package bsp1;

import java.awt.*;

public class Circle implements Shape{

    private final AlreadyImplementedCircle circle;

    public Circle(AlreadyImplementedCircle circle) {
        this.circle = circle;
    }

    @Override
    public void display() {
        circle.showCircle();
    }

    @Override
    public void setColor(Color c) {
        circle.changeColor(c);
    }

    @Override
    public void scale(float factor) {
        circle.scale(factor, factor);
    }
}
