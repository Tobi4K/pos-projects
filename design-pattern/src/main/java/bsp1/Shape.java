package bsp1;

import java.awt.*;

public interface Shape {
    void display();
    void setColor(Color c);
    void scale(float factor);
}
