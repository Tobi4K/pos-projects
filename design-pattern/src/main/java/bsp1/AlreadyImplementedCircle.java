package bsp1;

import java.awt.*;

public class AlreadyImplementedCircle {
    void showCircle() {
        System.out.println("showing circle");
    }

    void changeColor(Color color) {
        System.out.println("changing color to " + color);
    }

    void scale(float width, float height) {
        System.out.println("scaling to " + width + "x" + height);
    }

}
