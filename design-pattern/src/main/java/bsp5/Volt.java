package bsp5;

public class Volt {
    private int volt;

    Volt(int volt) {
        this.volt = volt;
    }

    void setVolt(int volt) {
        this.volt = volt;
    }

    public int getVolt() {
        return volt;
    }
}