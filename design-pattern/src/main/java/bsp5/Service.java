package bsp5;

public class Service implements ITriSocket {

    @Override
    public Volt get120Volt() {
        return new Socket_120().getVolt();
    }

    @Override
    public Volt get12Volt() {
        var socket = new Socket_120();
        socket.transform(12);
        return socket.getVolt();
    }

    @Override
    public Volt get3Volt() {
        var socket = new Socket_120();
        socket.transform(3);
        return socket.getVolt();
    }
}
