package bsp5;

public interface ITriSocket {
    Volt get120Volt();

    Volt get12Volt();

    Volt get3Volt();
}