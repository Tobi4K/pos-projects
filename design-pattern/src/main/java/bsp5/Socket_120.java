package bsp5;

public class Socket_120 {
    private Volt volt;

    public Socket_120() {
        this.volt = new Volt(120);
    }

    public void transform(int volt) {
        this.volt.setVolt(volt);
    }

    public Volt getVolt() {
        return this.volt;
    }
}
