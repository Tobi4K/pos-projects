package bsp6;

public class SoccerTeam {

    private Formation formation;
    public SoccerTeam() {
    }

    public void newFormation(Formation formation) {
        this.formation = formation;
    }

    public void playGame() {
        if (formation != null)
            formation.play();
        else
            throw new RuntimeException("Formation needs to be set first");
    }
}
