package bsp6;

public class App {
    public static void main(String[] args) {
        var team = new SoccerTeam();

        team.newFormation(new Formation_433());
        team.playGame();

        team.newFormation(new Formation_442());
        team.playGame();
    }
}
