package at.tobaisnolz.test_project.presentation.web;

import at.tobaisnolz.test_project.persistence.EmployeeRepository;
import at.tobaisnolz.test_project.persistence.TaskRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/employees")
public record TaskManagementController(EmployeeRepository employeeRepository,
                                       TaskRepository taskRepository) {

    @GetMapping
    public String getEmployees(Model model) {
        var employees = employeeRepository.findAll();
        model.addAttribute("employees", employees);
        return "all";
    }

    @GetMapping("/tasks")
    public String getTasks(@RequestParam String id, Model model) {
        var tasks = taskRepository.findAllByEmployee_Id(id);
        model.addAttribute("tasks", tasks);
        return "table";
    }

}
