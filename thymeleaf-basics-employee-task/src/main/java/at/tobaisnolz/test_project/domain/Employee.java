package at.tobaisnolz.test_project.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "staff")
public class Employee {

    @Id
    @Length(max = 6)
    private String id;

    private String firstName;

    private String lastName;

    @OneToMany(mappedBy = "employee")
    @JsonIgnore
    private List<Task> tasks = new ArrayList<>();
}
