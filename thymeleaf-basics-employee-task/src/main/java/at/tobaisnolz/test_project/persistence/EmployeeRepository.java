package at.tobaisnolz.test_project.persistence;

import at.tobaisnolz.test_project.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, String> {
}
