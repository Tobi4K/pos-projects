package at.tobaisnolz.test_project.persistence;

import at.tobaisnolz.test_project.domain.Task;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TaskRepository extends JpaRepository<Task, Integer> {
    List<Task> findAllByEmployee_Id(String id);
}
