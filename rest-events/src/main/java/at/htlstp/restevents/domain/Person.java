package at.htlstp.restevents.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "persons")

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Column(name = "person_name")
    private String name;

    @JsonIgnore
    @ManyToMany(mappedBy = "persons")
    private List<Event> events = new ArrayList<>();

    public void addEvent(Event event) {
        var conflicting = events.stream()
                .anyMatch(eventToCheck -> {
                    var beginConflicting =
                            event.getBegin().isBefore(eventToCheck.getEnd()) &&
                            event.getBegin().isAfter(eventToCheck.getBegin());

                    if (beginConflicting)
                        return true;

                    var endConflicting =
                            event.getEnd().isBefore(eventToCheck.getEnd()) &&
                            event.getEnd().isAfter(eventToCheck.getBegin());

                    return endConflicting;
                });

        if (conflicting)
            throw new IllegalArgumentException("Event conflicting with other event");

        events.add(event);
        event.getPersons().add(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return id.equals(person.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
