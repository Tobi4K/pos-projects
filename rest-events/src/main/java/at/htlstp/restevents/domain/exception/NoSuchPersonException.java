package at.htlstp.restevents.domain.exception;

import java.util.NoSuchElementException;

public class NoSuchPersonException extends NoSuchElementException {

    public NoSuchPersonException(Long personId) {
        super("No person with \"" + personId + "\" found");
    }
}
