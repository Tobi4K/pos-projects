package at.htlstp.restevents.domain.exception;

public class EventAlreadyExistsException extends RuntimeException {
    public EventAlreadyExistsException(Long id) {
        super("Event with id \"" + id + "\" already exists");
    }
}
