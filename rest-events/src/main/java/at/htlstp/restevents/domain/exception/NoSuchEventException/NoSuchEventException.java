package at.htlstp.restevents.domain.exception.NoSuchEventException;

import java.util.NoSuchElementException;

public class NoSuchEventException extends NoSuchElementException {
    public NoSuchEventException(Long id) {
        super("No event with id \"" + id + "\" could be found");
    }
}
