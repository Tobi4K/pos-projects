package at.htlstp.restevents.persistence;

import at.htlstp.restevents.domain.Event;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventRepository extends JpaRepository<Event, Long> {

}
