package at.htlstp.restevents.presentation.api;

import at.htlstp.restevents.domain.Event;
import at.htlstp.restevents.domain.exception.EventAlreadyExistsException;
import at.htlstp.restevents.domain.exception.NoSuchEventException.NoSuchEventException;
import at.htlstp.restevents.persistence.EventRepository;
import at.htlstp.restevents.persistence.PersonRepository;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;

@RestController
public record EventController(EventRepository eventRepository,
                               PersonRepository personRepository) {

    @GetMapping("/events/{id}")
    public Event getEventById(@PathVariable("id") Long eventId) {
        return eventRepository.findById(eventId)
                .orElseThrow(() -> new NoSuchEventException(eventId));
    }

    @PostMapping("/events")
    public ResponseEntity<Event> save(@RequestBody @Valid Event event) {
        try {
            var saved = eventRepository.save(event);
            var uri = ServletUriComponentsBuilder
                    .fromCurrentRequest()
                    .path("/{id}")
                    .build(saved.getId());
            return ResponseEntity.created(uri)
                    .body(saved);
        } catch (DataIntegrityViolationException e) {
            throw new EventAlreadyExistsException(event.getId());
        }
    }
}
