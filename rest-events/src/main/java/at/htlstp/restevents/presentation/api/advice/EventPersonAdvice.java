package at.htlstp.restevents.presentation.api.advice;

import at.htlstp.restevents.domain.exception.EventAlreadyExistsException;
import org.springframework.http.HttpStatus;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.NoSuchElementException;

@RestControllerAdvice
public class EventPersonAdvice {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoSuchElementException.class)
    String handleNoSuchElement(NoSuchElementException e) {
        return e.getMessage();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(EventAlreadyExistsException.class)
    String handleUserAlreadyExists(EventAlreadyExistsException e) {
        return e.getMessage();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    String handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException e) {
        return "parameter(s) not in valid format";
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({
            IllegalArgumentException.class,
            JpaSystemException.class
    })
    String handleExceptions(Exception e) {
        return e.getMessage();
    }

//    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
//    @ExceptionHandler(Throwable.class)
//    String handleThrowable(Throwable t) {
//        System.err.println(t.getMessage());
//        return "An error occured";
//    }
}
