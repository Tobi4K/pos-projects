package at.htlstp.restevents.presentation.api;

import at.htlstp.restevents.domain.Event;
import at.htlstp.restevents.domain.exception.NoSuchEventException.NoSuchEventException;
import at.htlstp.restevents.domain.exception.NoSuchPersonException;
import at.htlstp.restevents.persistence.EventRepository;
import at.htlstp.restevents.persistence.PersonRepository;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public record PersonController(EventRepository eventRepository,
                               PersonRepository personRepository) {

    @GetMapping("/persons/events")
    private List<Event> getEventsByPerson(@RequestParam("personId") Integer personId) {
        var person = personRepository.findById(personId.longValue())
                .orElseThrow(() -> new NoSuchPersonException(personId.longValue()));

        return person.getEvents();
    }

    @PostMapping("/persons/{id}/events")
    private void addEventToPerson(@PathVariable("id") Integer personId,
                                  @RequestBody @Valid Event event) {

        var person = personRepository.findById(personId.longValue())
                .orElseThrow(() -> new NoSuchPersonException(personId.longValue()));

        var savedEvent = eventRepository.findById(event.getId())
                .orElseThrow(() -> new NoSuchEventException(event.getId()));

        person.addEvent(savedEvent);
        eventRepository().save(savedEvent);
    }

}
