package at.tobiasnolz.personinterestmaturapractice.domain;

import lombok.*;
import org.hibernate.annotations.Fetch;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Entity

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Person implements Serializable {

    private static final String NAME_PATTERN = "[A-Z]\\w{0,29}";
    private static final String NAME_PATTERN_MESSAGE = "Name must start with a capital letter and following a maximum of 29 letters";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Pattern(regexp = NAME_PATTERN, message = NAME_PATTERN_MESSAGE)
    private String firstName;

    @Pattern(regexp = NAME_PATTERN, message = NAME_PATTERN_MESSAGE)
    private String lastName;

    @NotNull
    @PastOrPresent
    private LocalDate dateOfBirth;

    @ManyToMany()
    @Size(max = 3)
    @ToString.Exclude
    private Set<Interest> interests = new HashSet<>();

    @Enumerated(EnumType.ORDINAL)
    private Sex sex;

    public Person(String firstName, String lastName, LocalDate dateOfBirth, Sex sex) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.sex = sex;
    }

    public void addInterest(Interest interest) {
        this.interests.add(interest);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return id.equals(person.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public String getInterestString() {
        return interests.stream()
                .map(Interest::getDescription)
                .collect(Collectors.joining(", "));
    }
}
