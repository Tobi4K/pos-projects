package at.tobiasnolz.personinterestmaturapractice.domain;

public enum Sex {
    MALE, FEMALE, DIVERSE;
}
