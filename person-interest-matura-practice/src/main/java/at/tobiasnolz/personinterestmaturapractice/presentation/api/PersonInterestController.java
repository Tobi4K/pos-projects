package at.tobiasnolz.personinterestmaturapractice.presentation.api;

import at.tobiasnolz.personinterestmaturapractice.domain.Person;
import at.tobiasnolz.personinterestmaturapractice.domain.Sex;
import at.tobiasnolz.personinterestmaturapractice.persistence.InterestRepository;
import at.tobiasnolz.personinterestmaturapractice.persistence.PersonRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("persons")
public record PersonInterestController(
        InterestRepository interestRepository,
        PersonRepository personRepository
) {

    @GetMapping()
    public String getOverview(Model model) {
        var persons = personRepository.findAll();
        model.addAttribute("persons", persons);
        return "overview";
    }

    @GetMapping("/new")
    public String getPersonNew(Model model) {
        model.addAttribute("person", new Person());
        setDefaultModelAttributes(model);
        return "new";
    }

    @PostMapping("/new")
    public String savePersonNew(
            @Valid @ModelAttribute("person") Person person,
            BindingResult bindingResult,
            Model model
    ) {

        if (bindingResult.hasErrors()) {
            setDefaultModelAttributes(model);
            return "new";
        }

        personRepository.save(person);
        return "redirect:/persons";
    }

    private void setDefaultModelAttributes(Model model) {
        model.addAttribute("sexes", Sex.values());
        model.addAttribute("interests", interestRepository.findAll());
    }

}
