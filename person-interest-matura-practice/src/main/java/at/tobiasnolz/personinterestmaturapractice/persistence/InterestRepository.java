package at.tobiasnolz.personinterestmaturapractice.persistence;

import at.tobiasnolz.personinterestmaturapractice.domain.Interest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InterestRepository extends JpaRepository<Interest, Integer> {
}
