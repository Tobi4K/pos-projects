package at.tobiasnolz.personinterestmaturapractice.service;

import at.tobiasnolz.personinterestmaturapractice.domain.Interest;
import at.tobiasnolz.personinterestmaturapractice.domain.Person;
import at.tobiasnolz.personinterestmaturapractice.domain.Sex;
import at.tobiasnolz.personinterestmaturapractice.persistence.InterestRepository;
import at.tobiasnolz.personinterestmaturapractice.persistence.PersonRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public record DatabaseInitializer(
        InterestRepository interestRepository,
        PersonRepository personRepository
) implements CommandLineRunner {
    @Override
    public void run(String... args) {
        setupDB();
    }

    private void setupDB() {
        var interests = persistIntersts();
        persistPersons(interests);
    }

    private List<Person> persistPersons(List<Interest> interests) {
        var persons = List.of(
                new Person("Tobias", "Nolz", LocalDate.of(2002, 9, 10), Sex.MALE),
                new Person("Michael", "Gail", LocalDate.of(2003, 1, 1), Sex.MALE)
        );

        persons.get(0).addInterest(interests.get(0));
        persons.get(0).addInterest(interests.get(1));
        persons.get(0).addInterest(interests.get(2));

        return personRepository.saveAll(persons);
    }

    private List<Interest> persistIntersts() {
        var interests = List.of(
                new Interest("Reading"),
                new Interest("Trains"),
                new Interest("Soccer"),
                new Interest("Gaming"),
                new Interest("Programming"),
                new Interest("Crypto")
        );
        return interestRepository.saveAll(interests);
    }
}
