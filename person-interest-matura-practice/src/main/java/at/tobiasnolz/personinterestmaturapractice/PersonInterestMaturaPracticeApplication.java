package at.tobiasnolz.personinterestmaturapractice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PersonInterestMaturaPracticeApplication {

    public static void main(String[] args) {
        SpringApplication.run(PersonInterestMaturaPracticeApplication.class, args);
    }

}
