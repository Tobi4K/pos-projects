import org.junit.jupiter.api.Test;

import java.time.DayOfWeek;
import java.util.*;

import static java.util.Map.entry;
import static org.assertj.core.api.Assertions.assertThat;

public class Maps {

    @Test
    void HashMap() {
        var hashMap = new HashMap<Integer, String>();
        hashMap.put(1, "one");
        hashMap.put(2, "two");
        hashMap.put(3, "three");

        assertThat(hashMap)
                .isInstanceOf(AbstractMap.class)
                .hasSize(3);

        assertThat(hashMap.keySet())
                .contains(1, 2, 3);

        assertThat(hashMap.values())
                .contains("one", "two", "three");

        assertThat(hashMap.get(1))
                .isEqualTo("one");

        assertThat(hashMap.remove(1))
                .isEqualTo("one");

        assertThat(hashMap.getOrDefault(-1, "default"))
                .isEqualTo("default");

        hashMap.merge(2, "one", (oldValue, newValue) -> oldValue + newValue);
        assertThat(hashMap.get(2))
                .isEqualTo("twoone");
    }

    @Test
    void TreeMap() {
        var treeMap = new TreeMap<Integer, String>(Comparator.reverseOrder());
        treeMap.put(1, "one");
        treeMap.put(3, "three");
        treeMap.put(2, "two");

        assertThat(treeMap)
                .isInstanceOf(SortedMap.class)
                .hasSize(3);

        assertThat(treeMap)
                .containsExactly(
                        entry(3, "three"),
                        entry(2, "two"),
                        entry(1, "one")
                );

        assertThat(treeMap.descendingMap())
                .containsExactly(
                        entry(1, "one"),
                        entry(2, "two"),
                        entry(3, "three")
                );

        assertThat(treeMap.keySet())
                .containsExactly(3, 2, 1);

        assertThat(treeMap.values())
                .containsExactly("three", "two", "one");

    }

    @Test
    void EnumMap() {
        var enumMap = new EnumMap<DayOfWeek, String>(DayOfWeek.class);
        enumMap.put(DayOfWeek.MONDAY, "Monday");
        enumMap.put(DayOfWeek.TUESDAY, "Tuesday");
        enumMap.put(DayOfWeek.WEDNESDAY, "Wednesday");

        assertThat(enumMap)
                .isInstanceOf(Map.class)
                .hasSize(3);

        assertThat(enumMap)
                .contains(
                        entry(DayOfWeek.MONDAY, "Monday"),
                        entry(DayOfWeek.TUESDAY, "Tuesday"),
                        entry(DayOfWeek.WEDNESDAY, "Wednesday")
                );
    }
}

