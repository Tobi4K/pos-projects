import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class JUnit {

    @Nested
    class BeforeAfterClass {
        static Logger logger;

        @BeforeAll
        static void beforeAll() {
            logger = Logger.getLogger(JUnit.class.getName());
            logger.info("started testing");
        }


        @AfterAll
        static void afterAll() {
            logger.info("finished testing");
        }

        @Test
        void test1() {
            assertEquals(1, 1);
        }
    }

    @Nested
    class BeforeAfterTest {

        List<String> objectsToTest = new ArrayList<>();

        @BeforeEach
        void setUp() {
            objectsToTest.addAll(List.of("a", "b"));
        }

        @AfterEach
        void tearDown() {
            objectsToTest.clear();
        }

        @Test
        void test1() {
            assertEquals(2, objectsToTest.size());
        }
    }

    @Nested
    class AssertJ_demo {
        @Test
        void assertEquals_demo() {
            var result = 1 + 1;
            var expected = 2;

            assertEquals(expected, result);
        }

        @Test
        void assertTrueFalse_demo() {
            var trueBooleanOperation = 1 == 1;
            assertTrue(trueBooleanOperation);

            var falseBooleanOperation = 1 != 1;
            assertFalse(falseBooleanOperation);
        }

        @Test
        void assertNull_NotNull_demo() {
            String nullObject = null;
            assertNull(nullObject);

            String notNullObject = "not null";
            assertNotNull(notNullObject);
        }

        @Test
        void assertSame_NotSame_demo() {
            var string1 = "string1";
            var string2 = "string2";

            assertSame(string1, string1);
            assertNotSame(string1, string2);
        }

        @Test
        void assertThrows_demo() {
            assertThrows(ArithmeticException.class, () -> {
                var result = 1 / 0;
            });
        }

        @Test
        void assertThrows_withMessage_demo() {
            var thrown = assertThrows(Exception.class, () -> {
                throw new Exception("This is an exception");
            });

            assertTrue(thrown.getMessage().contains("This is an exception"));
        }

        @Test
        void assertThat_demo() {
            var string = "This is a string";
            assertThat(string)
                    .contains("string")
                    .startsWith("This")
                    .hasSizeGreaterThan(5);

            var list = List.of("a", "b", "c");
            assertThat(list)
                    .contains("b", "a")
                    .containsExactly("a", "b", "c")
                    .hasSize(3)
                    .isInstanceOf(List.class);
        }
    }

}
