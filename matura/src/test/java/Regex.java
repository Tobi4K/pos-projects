import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

public class Regex {

    @Nested
    class Expressions {

        @Test
        void test_isValidDate() {
            var dateRegex = "[0-9]{2}\\.[0-9]{2}\\.[0-9]{4}";

            assertThat("01.01.2020".matches(dateRegex))
                    .isTrue();

            assertThat("101.01.2020".matches(dateRegex))
                    .isFalse();

            assertThat("test 11.01.2020".matches(dateRegex))
                    .isFalse();
        }

        @Test
        void test_wordClasses() {
            var digitsRegex = "\\d+";
            assertAll(
                    () -> assertThat("123".matches(digitsRegex)).isTrue(),
                    () -> assertThat("abc".matches(digitsRegex)).isFalse()
            );

            var wordsRegex = "\\w+";
            assertAll(
                    () -> assertThat("123".matches(wordsRegex)).isTrue(),
                    () -> assertThat("abc".matches(wordsRegex)).isTrue(),
                    () -> assertThat("_".matches(wordsRegex)).isTrue(),
                    () -> assertThat("&".matches(wordsRegex)).isFalse()
            );

            var whiteSpaceRegex = "\\s+";
            assertAll(
                    () -> assertThat(" ".matches(whiteSpaceRegex)).isTrue(),
                    () -> assertThat("123".matches(whiteSpaceRegex)).isFalse(),
                    () -> assertThat("abc".matches(whiteSpaceRegex)).isFalse(),
                    () -> assertThat("_".matches(whiteSpaceRegex)).isFalse()
            );
        }

        @Test
        void test_negation() {
            var digitsRegex = "\\D+";
            assertAll(
                    () -> assertThat("123".matches(digitsRegex)).isFalse(),
                    () -> assertThat("abc".matches(digitsRegex)).isTrue()
            );

            var otherNegation = "[^abc]*";
            assertAll(
                    () -> assertThat("def".matches(otherNegation)).isTrue(),
                    () -> assertThat("test".matches(otherNegation)).isTrue(),
                    () -> assertThat("a test".matches(otherNegation)).isFalse()
            );
        }

        @Test
        void test_characterBetween() {
            var characterBetween = "[a-zA-Z]";
            assertAll(
                    () -> assertThat("a".matches(characterBetween)).isTrue(),
                    () -> assertThat("z".matches(characterBetween)).isTrue(),
                    () -> assertThat("A".matches(characterBetween)).isTrue(),
                    () -> assertThat("Z".matches(characterBetween)).isTrue(),
                    () -> assertThat("0".matches(characterBetween)).isFalse(),
                    () -> assertThat("9".matches(characterBetween)).isFalse()
            );
        }

        @Test
        void test_quantifiers() {
            var betweenQuantifier = "\\d{2,3}";
            assertAll(
                    () -> assertThat("1".matches(betweenQuantifier)).isFalse(),
                    () -> assertThat("12".matches(betweenQuantifier)).isTrue(),
                    () -> assertThat("123".matches(betweenQuantifier)).isTrue(),
                    () -> assertThat("1234".matches(betweenQuantifier)).isFalse()
            );

            var exactQuantifier = "\\d{2}";
            assertAll(
                    () -> assertThat("1".matches(exactQuantifier)).isFalse(),
                    () -> assertThat("12".matches(exactQuantifier)).isTrue(),
                    () -> assertThat("123".matches(exactQuantifier)).isFalse()
            );

            var minQuantifier = "\\d{2,}";
            assertAll(
                    () -> assertThat("1".matches(minQuantifier)).isFalse(),
                    () -> assertThat("12".matches(minQuantifier)).isTrue(),
                    () -> assertThat("123".matches(minQuantifier)).isTrue()
            );

            var anyQuantifier = "\\d*";
            assertAll(
                    () -> assertThat("".matches(anyQuantifier)).isTrue(),
                    () -> assertThat("1".matches(anyQuantifier)).isTrue(),
                    () -> assertThat("12".matches(anyQuantifier)).isTrue(),
                    () -> assertThat("123".matches(anyQuantifier)).isTrue()
            );

            var atLeastOneQuantifier = "\\d+";
            assertAll(
                    () -> assertThat("".matches(atLeastOneQuantifier)).isFalse(),
                    () -> assertThat("1".matches(atLeastOneQuantifier)).isTrue(),
                    () -> assertThat("12".matches(atLeastOneQuantifier)).isTrue(),
                    () -> assertThat("123".matches(atLeastOneQuantifier)).isTrue()
            );

            var zeroOrOneQuantifier = "\\d?";
            assertAll(
                    () -> assertThat("".matches(zeroOrOneQuantifier)).isTrue(),
                    () -> assertThat("1".matches(zeroOrOneQuantifier)).isTrue(),
                    () -> assertThat("12".matches(zeroOrOneQuantifier)).isFalse()
            );
        }

        @Test
        void test_or() {
            var orRegex = "ab|cd";
            assertAll(
                    () -> assertThat("ab".matches(orRegex)).isTrue(),
                    () -> assertThat("cd".matches(orRegex)).isTrue(),
                    () -> assertThat("de".matches(orRegex)).isFalse()
            );
        }

        @Test
        void test_anyCharacter() {
            var orRegex = "ab.*cd";
            assertAll(
                    () -> assertThat("ab test cd".matches(orRegex)).isTrue(),
                    () -> assertThat("abcd".matches(orRegex)).isTrue(),
                    () -> assertThat("test".matches(orRegex)).isFalse()
            );
        }
    }


    @Nested
    class MatcherTest {

        private Matcher matcher;

        @BeforeEach
        void setUp() {
            var regex = "(test)";
            var testString = "I am a test string for testing";
            matcher = Pattern.compile(regex).matcher(testString);
        }

        @Test
        void test_matcher_findStartEnd() {
            assertAll(
                    () -> assertThat(matcher.find()).isTrue(),
                    () -> assertThat(matcher.start()).isEqualTo(7),
                    () -> assertThat(matcher.end()).isEqualTo(11)
            );

            assertAll(
                    () -> assertThat(matcher.find()).isTrue(),
                    () -> assertThat(matcher.start()).isEqualTo(23),
                    () -> assertThat(matcher.end()).isEqualTo(27)
            );
        }

        @Test
        void test_matcher_replace() {
            assertThat(matcher.replaceAll("FOUND_TEST"))
                    .isEqualTo("I am a FOUND_TEST string for FOUND_TESTing");
        }
    }

}


