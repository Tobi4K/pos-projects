import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LocalDateTimeDemo {

    @Test
    void test_local_date() {

        var date = LocalDate.now();
        System.out.println(date.format(DateTimeFormatter.ISO_DATE));
        System.out.println(date.format(DateTimeFormatter.BASIC_ISO_DATE));
        System.out.println(date.format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));

    }

    @Test
    void test_local_date_time() {

        var date = LocalDateTime.now();
        System.out.println(date.format(DateTimeFormatter.ISO_DATE));
        System.out.println(date.format(DateTimeFormatter.ISO_DATE_TIME));
        System.out.println(date.format(DateTimeFormatter.ofPattern("HH:mm:ss - dd.MM.yyyy")));

    }

}
