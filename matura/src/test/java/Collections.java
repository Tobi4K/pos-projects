import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class Collections {

    @Nested
    class ListInterface {

        @Test
        void arrayList() {
            var arrayList = new ArrayList<>(List.of("a", "b", "b", "c"));
            assertThat(arrayList)
                    .isInstanceOf(List.class)
                    .hasSize(4)
                    .containsExactly("a", "b", "b", "c");

            assertThat(arrayList.get(0))
                    .isEqualTo("a");

            assertThat(arrayList.remove(0))
                    .isEqualTo("a");
        }

        @Test
        void linkedList() {
            var linkedList = new LinkedList<>(List.of("a", "b", "b", "c"));
            assertThat(linkedList)
                    .isInstanceOf(List.class)
                    .isInstanceOf(Queue.class)
                    .hasSize(4)
                    .containsExactly("a", "b", "b", "c");

            assertThat(linkedList.pop()) // equal to .removeFrist()
                    .isEqualTo("a");

            linkedList.push("a1"); // equal to .addFirst()
            linkedList.add("d");

            assertThat(linkedList)
                    .containsExactly("a1", "b", "b", "c", "d");
        }

        @Test
        void stack() {
            var stack = new Stack<>();
            stack.addAll(List.of("a", "b", "b", "c"));

            assertThat(stack)
                    .isInstanceOf(List.class)
                    .isInstanceOf(Vector.class)
                    .hasSize(4)
                    .containsExactly("a", "b", "b", "c");

            assertThat(stack.pop())
                    .isEqualTo("c");

            stack.push("d");

            assertThat(stack)
                    .containsExactly("a", "b", "b", "d");
        }
    }

    @Nested
    class QueueInterface {
        @Test
        void priorityQueue() {
            var priorityQueue = new PriorityQueue<String>(Comparator.reverseOrder());
            priorityQueue.addAll(List.of("a", "b", "b", "c"));

            assertThat(priorityQueue)
                    .isInstanceOf(Queue.class)
                    .isNotInstanceOf(List.class)
                    .hasSize(4)
                    .containsExactly("c", "b", "b", "a");

            assertThat(priorityQueue.poll())
                    .isEqualTo("c");

            priorityQueue.add("d");

            assertThat(priorityQueue)
                    .containsExactly("d", "b", "b", "a");

        }
    }

    @Nested
    class SetInterface {
        @Test
        void hashSet() {
            var hashSet = new HashSet<>(List.of("a", "b", "b", "c"));
            assertThat(hashSet)
                    .isInstanceOf(Set.class)
                    .hasSize(3)
                    .contains("a", "b", "c");

            assertThat(hashSet.remove("a"))
                    .isTrue();

            assertThat(hashSet)
                    .containsExactly("b", "c");
        }

        @Test
        void treeSet() {
            var treeSet = new TreeSet<String>(Comparator.reverseOrder());
            treeSet.addAll(List.of("a", "b", "b", "c"));

            assertThat(treeSet)
                    .isInstanceOf(Set.class)
                    .isInstanceOf(SortedSet.class)
                    .hasSize(3)
                    .containsExactly("c", "b", "a");

            assertThat(treeSet.remove("b"))
                    .isTrue();

            assertThat(treeSet)
                    .containsExactly("c", "a");
        }

    }

    @Nested
    class Iterator {

        @Test
        void test_remove_fails() {
            var list = new LinkedList<>(List.of(1, 2, 3, 4, 5, 6, 7));
            assertThrows(
                    ConcurrentModificationException.class,
                    () -> list.stream()
                            .filter(element -> element > 5)
                            .forEach(list::remove)
            );
        }

        @Test
        void test_remove_during_iteration() {
            var list = new LinkedList<>(List.of(1, 2, 3, 4, 5, 6, 7));
            var iterator = list.iterator();
            while (iterator.hasNext()) {
                var element = iterator.next();
                if (element > 5)
                    iterator.remove();
            }

            assertThat(list)
                    .contains(1, 2, 3, 4, 5);
        }

        @Test
        void test_remove_with_removeIf() {
            var list = new LinkedList<>(List.of(1, 2, 3, 4, 5, 6, 7));
            list.removeIf(element -> element > 5);

            assertThat(list)
                    .contains(1, 2, 3, 4, 5);
        }
    }
}
