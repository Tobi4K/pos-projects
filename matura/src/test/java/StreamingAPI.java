import app.domain.SchoolClass;
import app.domain.Student;
import app.domain.Teacher;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.Map.entry;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.InstanceOfAssertFactories.DATE;
import static org.assertj.core.api.InstanceOfAssertFactories.spliterator;

public class StreamingAPI {

    @Nested
    class TestStringJoiner {

        private Collection<Teacher> teachers;

        @BeforeEach
        void setup() {
            teachers = List.of(
                    new Teacher("id1", "Tobias", "Nolz"),
                    new Teacher("id2", "Michael", "Gail"),
                    new Teacher("id3", "Benedikt", "Schrabauer")
            );
        }

        @Test
        void string_joine_class() {
            var result = teachers
                    .stream()
                    .map(teacher -> teacher.getFirstName() + " " + teacher.getLastName())
                    .collect(() -> new StringJoiner(", "),
                            StringJoiner::add,
                            StringJoiner::merge
                    ).toString();

            assertThat(result)
                    .isEqualTo("Tobias Nolz, Michael Gail, Benedikt Schrabauer");
        }

        @Test
        void collectors_joining() {
            var result = teachers
                    .stream()
                    .map(teacher -> teacher.getFirstName() + " " + teacher.getLastName())
                    .collect(Collectors.joining(", "));

            assertThat(result)
                    .isEqualTo("Tobias Nolz, Michael Gail, Benedikt Schrabauer");
        }
    }

    @Nested
    class ComparatorTest {

        private List<SchoolClass> schoolClasses;

        @BeforeEach
        void setUp() {
            schoolClasses = List.of(
                    new SchoolClass("5AHIF"),
                    new SchoolClass("5BHIF"),
                    new SchoolClass("5CHIF")
            );

            schoolClasses.get(0)
                    .addStudent(new Student());

            schoolClasses.get(0)
                    .addStudent(new Student());

            schoolClasses.get(1)
                    .addStudent(new Student());

            schoolClasses.get(2)
                    .addTeacher(new Teacher());
        }

        @Test
        void not_comparable() {
            assertThatThrownBy(() ->
                    schoolClasses.stream()
                            .sorted()
                            .collect(Collectors.toList()))
                    .hasMessageContaining("Comparable");
        }

        @Test
        void sort_alphabetic() {
            var result = schoolClasses.stream()
                    .sorted(Comparator.comparing(SchoolClass::getName))
                    .map(SchoolClass::getName);

            assertThat(result)
                    .containsExactly("5AHIF", "5BHIF", "5CHIF");
        }

        @Test
        void sort_by_multiple_variables() {
            var result = schoolClasses.stream()
                    .sorted(Comparator.comparing(SchoolClass::getTeachers, Comparator.comparingInt(List::size))
                            .thenComparing(SchoolClass::getStudents, ((o1, o2) -> Integer.compare(o2.size(), o1.size())))
                            .thenComparing(SchoolClass::getName)
                    )
                    .map(SchoolClass::getName);

            assertThat(result)
                    .containsExactly("5AHIF", "5BHIF", "5CHIF");
        }

        @Test
        void manual_comparator() {
            var result = schoolClasses.stream()
                    .sorted((o1, o2) -> o2.getName().compareTo(o1.getName()))
                    .map(SchoolClass::getName);

            assertThat(result)
                    .containsExactly("5CHIF", "5BHIF", "5AHIF");
        }

    }

    @Nested
    class GroupByTest {

        @Test
        void group_by() {
            var result = Stream.of("5AHIF", "5BHIF", "5CHIF", "5AHIF", "5AHIF", "5BHIF")
                    .collect(Collectors.groupingBy(
                            Function.identity(),
                            Collectors.counting()
                    ));

            assertThat(result)
                    .contains(
                            entry("5AHIF", 3L),
                            entry("5BHIF", 2L),
                            entry("5CHIF", 1L)
                    );
        }

        @Test
        void to_map() {
            var result = Stream.of("5AHIF", "5BHIF", "5CHIF")
                    .collect(Collectors.toMap(
                            string -> string.charAt(1),
                            s -> s
                    ));

            assertThat(result)
                    .contains(
                            entry('A', "5AHIF"),
                            entry('B', "5BHIF"),
                            entry('C', "5CHIF")
                    );
        }


    }

    @Nested
    class Reduce {

        @Test
        void test_reduce_compare() {
            var result = Stream.of("A", "B", "C", "D")
                    .reduce((v1, v2) -> v1.compareTo(v2) > 0 ? v1 : v2);

            assertThat(result)
                    .isPresent()
                    .get()
                    .isEqualTo("D");
        }

        @Test
        void test_reduce_append() {
            var result = Stream.of("A", "B", "C", "D")
                    .reduce((v1, v2) -> v1 + v2);

            assertThat(result)
                    .isPresent()
                    .get()
                    .isEqualTo("ABCD");
        }

        @Test
        void test_reduce_append_with_default() {
            var result = Stream.of("A", "B", "C", "D")
                    .reduce("Start: ", (v1, v2) -> v1 + v2);

            assertThat(result)
                    .isEqualTo("Start: ABCD");
        }

    }

    @Nested
    class FlatMap {

        @Test
        void test_flat_map() {
            var list1 = List.of("A", "B", "C");
            var list2 = List.of("D", "E", "F");

            var totalList = List.of(list1, list2);
            var combined = totalList.stream()
                    .flatMap(Collection::stream)
                    .toList();

            assertThat(combined)
                    .isEqualTo(List.of("A", "B", "C", "D", "E", "F"));
        }

        @Test
        @SneakyThrows
        void test_flat_map_csv() {

            var path = Path.of(getClass().getResource("/numbers.csv").toURI());

            try (var lines = Files.lines(path)) {

                var result = lines.map(line -> line.split(","))
                        .flatMap(Arrays::stream)
                        .map(Integer::valueOf)
                        .distinct()
                        .sorted()
                        .toList();

                var expected = IntStream.rangeClosed(1, 10)
                        .boxed()
                        .toList();

                assertThat(result)
                        .isEqualTo(expected);
            }

        }

    }
}
