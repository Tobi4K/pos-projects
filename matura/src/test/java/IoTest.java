import app.domain.Teacher;
import lombok.Data;
import lombok.SneakyThrows;
import org.junit.jupiter.api.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.RandomAccess;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.in;
import static org.junit.jupiter.api.Assertions.assertAll;

public class IoTest {

    @Nested
    class ReadingTextFiles {

        @Test
        @SneakyThrows
        void works_as_pom_sibling() {
            var path = Path.of("input-bad.txt");
            var lines = Files.readAllLines(path);

            assertThat(lines)
                    .hasSize(3);
        }

        @Test
        @SneakyThrows
        void works_from_resources() {
            var path = Path.of(getClass().getResource("/input.txt").toURI());
            var lines = Files.readAllLines(path);

            assertThat(lines)
                    .hasSize(3);
        }

        @Test
        @SneakyThrows
        void with_any_length() {
            var path = Path.of(getClass().getResource("/input.txt").toURI());

            try (var lines = Files.lines(path)) {
                assertThat(lines)
                        .hasSize(3);
            }
        }

        @Test
        @SneakyThrows
        void with_readers() {
            try (
                    var stream = getClass().getResourceAsStream("/input.txt");
                    var reader = new BufferedReader(new InputStreamReader(stream))
            ) {
                assertThat(reader.lines())
                        .hasSize(3);
            }
        }
    }

    @Nested
    class WritingTextFiles {

        @Test
        @SneakyThrows
        void with_Files() {
            var path = Path.of("output.txt");

            Files.writeString(path, "Ja\n");
            Files.writeString(path, "Nein\n", StandardOpenOption.APPEND);
            Files.writeString(path, "Vielleicht\n", StandardOpenOption.APPEND);

            assertThat(Files.readAllLines(path))
                    .hasSize(3);
        }

        @Test
        @SneakyThrows
        void with_Streams() {
            var path = Path.of("output.txt");

            try (var writer = new PrintWriter(new FileWriter("output.txt"))) {
                writer.println("1");
                writer.println("2");
                writer.println("3");
            }

            assertThat(Files.readAllLines(path))
                    .hasSize(3);
        }
    }

    @Nested
    class WritingBinaryFiles {

        @Test
        @SneakyThrows
        void with_raf() {
            try (var file = new RandomAccessFile("output.bin", "rw")) {
                file.writeLong(42L);
                file.writeUTF("Hallo");
                file.writeDouble(3.1415);
            }

            assertThat(new File("output.bin").length())
                    .isEqualTo(23);
        }

        @Test
        @SneakyThrows
        void with_streams() {
            try (var stream = new DataOutputStream(new FileOutputStream("output.bin"))) {
                stream.writeLong(42L);
                stream.writeUTF("Hallo");
                stream.writeDouble(3.1415);
            }

            assertThat(new File("output.bin").length())
                    .isEqualTo(23);
        }
    }

    @Nested
    class ReadingBinaryFiles {

        @Test
        @SneakyThrows
        void with_raf() {
            var contents = new ArrayList<Object>();

            try (var file = new RandomAccessFile("output.bin", "r")) {
                contents.add(file.readLong());
                contents.add(file.readUTF());
                contents.add(file.readDouble());
            }

            assertThat(contents)
                    .containsExactly(42L, "Hallo", 3.1415);
        }

        @Test
        @SneakyThrows
        void with_streams() {
            var contents = new ArrayList<Object>();

            try (var stream = new DataInputStream(new FileInputStream("output.bin"))) {
                contents.add(stream.readLong());
                contents.add(stream.readUTF());
                contents.add(stream.readDouble());
            }

            assertThat(contents)
                    .containsExactly(42L, "Hallo", 3.1415);
        }
    }

    @Nested
    @TestMethodOrder(MethodOrderer.OrderAnnotation.class)
    class Binary {

        private static final String fileName = "test.bin";

        @BeforeAll
        @SneakyThrows
        static void beforeAll() {
            afterAll();
            Files.createFile(Path.of(fileName));
        }

        @AfterAll
        @SneakyThrows
        static void afterAll() {
            Files.deleteIfExists(Path.of(fileName));
        }

        @Test
        @Order(1)
        void writeBinary() throws IOException {

            try (
                    var fileOutputStream = new FileOutputStream(fileName);
                    var bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
                    var dataOutputStream = new DataOutputStream(bufferedOutputStream);
                    var objectOutputStream = new ObjectOutputStream(bufferedOutputStream);
            ) {

                dataOutputStream.writeLong(1L);
                dataOutputStream.writeUTF("Hallo, ich bin ein Test");
                objectOutputStream.writeObject(new Teacher("nolz", "Tobias", "Nolz"));

            }

        }

        @Test
        @Order(2)
        @SneakyThrows
        void readBinary() {

            var contents = new ArrayList<>();
            try (
                    var fileInputStream = new FileInputStream(fileName);
                    var bufferedInputStream = new BufferedInputStream(fileInputStream);
                    var dataInputStream = new DataInputStream(bufferedInputStream);
                    var objectInputStream = new ObjectInputStream(bufferedInputStream)
            ) {

                contents.add(dataInputStream.readLong());
                contents.add(dataInputStream.readUTF());
                contents.add(objectInputStream.readObject());

            }

            assertAll(
                    () -> assertThat(contents.get(0)).isEqualTo(1L),
                    () -> assertThat(contents.get(1)).isEqualTo("Hallo, ich bin ein Test"),
                    () -> assertThat(contents.get(2)).isInstanceOf(Teacher.class),
                    () -> assertThat((Teacher) contents.get(2)).isEqualTo(new Teacher("nolz", "Tobias", "Nolz"))
            );

        }

        @Test
        @Order(3)
        @SneakyThrows
        void write_RAF() {
            try (var raf = new RandomAccessFile(fileName, "rw")) {

                for (int i = 0; i < 100; i++) {
                    raf.writeInt(i);
                }

            }
        }

        @Test
        @Order(4)
        @SneakyThrows
        void read_RAF() {
            try (var raf = new RandomAccessFile(fileName, "r")) {

                int i = 0;
                while (raf.length() > raf.getFilePointer()) {
                    assertThat(raf.readInt()).isEqualTo(i++);
                }

                assertThat(i).isEqualTo(100);
            }
        }
    }

    @Nested
    @TestMethodOrder(MethodOrderer.OrderAnnotation.class)
    class ByteStream {

        private static byte[] bytes;

        @Test
        @SneakyThrows
        @Order(1)
        void write_array() {

            try (var byteArrayOutputStream = new ByteArrayOutputStream();
                 var outputStreamWriter = new OutputStreamWriter(byteArrayOutputStream);
                 var bufferedWriter = new BufferedWriter(outputStreamWriter);
                 var printWriter = new PrintWriter(bufferedWriter)) {

                printWriter.println("Hallo, ich bin ein Test");
                printWriter.println("Hallo, ich bin ein Test2");
                printWriter.print(123L);

                bufferedWriter.flush();
                bytes = byteArrayOutputStream.toByteArray();
            }

        }

        @Test
        @SneakyThrows
        @Order(2)
        void read_array() {

            var contents = new ArrayList<>();
            try (var byteArrayInputStream = new ByteArrayInputStream(bytes);
                 var inputStreamReader = new InputStreamReader(byteArrayInputStream);
                 var bufferedReader = new BufferedReader(inputStreamReader)) {

                contents.add(bufferedReader.readLine());
                contents.add(bufferedReader.readLine());

            }

            assertAll(
                    () -> assertThat((String) contents.get(0)).isEqualTo("Hallo, ich bin ein Test"),
                    () -> assertThat((String) contents.get(1)).isEqualTo("Hallo, ich bin ein Test2")
            );

        }


    }

}
