create table teachers
(
    id         varchar(255) primary key,
    first_name varchar(255),
    last_name  varchar(255)
);

create table school_classes
(
    id varchar(255) primary key
);

create table students
(
    id              int primary key,
    first_name      varchar(255),
    last_name       varchar(255),
    birth_date      date,
    school_class_id varchar(255)
);

create table teacher_class
(
    class_id   varchar(255) not null,
    teacher_id varchar(255) not null
);

alter table teacher_class
    add primary key (class_id, teacher_id);

alter table teacher_class
    add foreign key (class_id) references school_classes (id);

alter table teacher_class
    add foreign key (teacher_id) references teachers (id);

alter table students
    add foreign key (school_class_id) references school_classes (id);