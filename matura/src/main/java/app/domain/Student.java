package app.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.apache.tomcat.jni.Local;
import org.hibernate.annotations.NaturalId;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Objects;

@Entity
@Table(name = "students")

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString

public class Student implements Serializable {

    @Id
    @NaturalId
//    @Pattern(regexp = "^\\d{8}$") -> with Id as String
    @Max(99999999)
    @Min(10000000)
    private Integer id;

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @PastOrPresent
    private LocalDate birthDate;

    @ManyToOne
    @JsonIgnore
    private SchoolClass schoolClass;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return id.equals(student.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @AssertTrue(message = "Year of student id cannot be greater than current year")
    private boolean isValidId() {
        var idYear = id.toString().substring(0, 4);
        var currentYear = LocalDate.now().getYear();
        return Integer.parseInt(idYear) <= currentYear;
    }

    public Student(Integer id, String firstName, String lastName, LocalDate birthDate) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
    }
}
