package app.domain.exceptions;

import java.util.NoSuchElementException;

public class NoSuchTeacherException extends NoSuchElementException {
    public NoSuchTeacherException(String teacherId) {
        super("No teacher found with id \"%s\"".formatted(teacherId));
    }
}
