package app.domain.exceptions;


public class TeacherAlreadyExistsException extends IllegalArgumentException {
    public TeacherAlreadyExistsException(String id) {
        super("Teacher with id \"%s\" already exists".formatted(id));
    }
}
