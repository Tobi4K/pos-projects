package app.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "teachers")

@AllArgsConstructor
@NoArgsConstructor

@Getter
@Setter
@ToString
public class Teacher implements Serializable {

    @Id
    @NaturalId
    @Pattern(regexp = "[a-z]{4}")
    private String id;

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    @ManyToMany
    @JoinTable(name = "teacher_class",
            joinColumns = @JoinColumn(name = "teacher_id"),
            inverseJoinColumns = @JoinColumn(name = "class_id"))
    @ToString.Exclude
    @JsonIgnore
    private List<SchoolClass> schoolClasses = new ArrayList<>();

    public void setSchoolClasses(List<SchoolClass> schoolClasses) {
        this.schoolClasses = schoolClasses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Teacher teacher = (Teacher) o;
        return id.equals(teacher.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Teacher(String id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
