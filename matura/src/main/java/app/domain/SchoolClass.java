package app.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "school_classes")

@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
public class SchoolClass implements Serializable {

    @Id
    @NaturalId
    @Pattern(regexp = "[1-5][A-C]HIF")
    @Column(name = "id")
    private String name;

    @OneToMany(mappedBy = "schoolClass")
    @ToString.Exclude
    private List<Student> students = new ArrayList<>();

    @ManyToMany(mappedBy = "schoolClasses")
    @ToString.Exclude
    private List<Teacher> teachers = new ArrayList<>();

    public void setTeachers(List<Teacher> teachers) {
        this.teachers = teachers;
        teachers.forEach(teacher -> teacher.getSchoolClasses().add(this));
    }

    public void addTeacher(Teacher teacher) {
        this.teachers.add(teacher);
        teacher.getSchoolClasses().add(this);
    }

    public void setStudents(List<Student> students) {
        this.students = students;
        students.forEach(student -> student.setSchoolClass(this));
    }

    public void addStudent(Student student) {
        students.add(student);
        student.setSchoolClass(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SchoolClass that = (SchoolClass) o;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    public SchoolClass(String name) {
        this.name = name;
    }
}
