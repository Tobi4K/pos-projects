package app.presentation.api;

import app.domain.Teacher;
import app.domain.exceptions.NoSuchTeacherException;
import app.domain.exceptions.TeacherAlreadyExistsException;
import app.persistence.TeacherRepository;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("api/teachers")
public record TeacherController(
        TeacherRepository teacherRepository
) {

    @GetMapping()
    public List<Teacher> getAll() {
        return teacherRepository.findAll();
    }

    @GetMapping("{id}")
    public Teacher getById(@PathVariable("id") String teacherId) {
        return teacherRepository.findById(teacherId)
                .orElseThrow(() -> new NoSuchTeacherException(teacherId));
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("{id}")
    public void deleteTeacher(@PathVariable("id") String teacherId) {
        try {
            teacherRepository.deleteById(teacherId);
        } catch (DataAccessException e) {
            throw new NoSuchTeacherException(teacherId);
        }
    }

    @PostMapping()
    public ResponseEntity<Teacher> saveTeacher(@RequestBody @Valid Teacher teacher) {
        if(teacherRepository.findById(teacher.getId()).isPresent())
            throw new TeacherAlreadyExistsException(teacher.getId());

        var saved = teacherRepository.save(teacher);
        return ResponseEntity
                .created(getCreatedUri(teacher.getId()))
                .body(saved);
    }

    @PutMapping("{id}")
    public ResponseEntity<Teacher> replaceTeacher(@PathVariable("id") String teacherId,
                                                  @RequestBody @Valid Teacher teacher) {
        var toSave = teacherRepository.findById(teacherId)
                .map(presentTeacher -> {
                    presentTeacher.setFirstName(teacher.getFirstName());
                    presentTeacher.setLastName(teacher.getLastName());
                    presentTeacher.setSchoolClasses(teacher.getSchoolClasses());
                    return presentTeacher;
                }).orElseGet(() -> {
                    teacher.setId(teacherId);
                    return teacher;
                });

        var saved = teacherRepository.save(toSave);

        boolean newStudentCreated = toSave == teacher;
        if (newStudentCreated) {
            var uri = getCreatedUri(teacher.getId());
            return ResponseEntity
                    .created(uri)
                    .body(saved);
        } else {
            return ResponseEntity
                    .ok(saved);
        }
    }

    private URI getCreatedUri(String id) {
        return ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("{id}")
                .build(id);
    }

}
