package app.setup;

import app.domain.SchoolClass;
import app.domain.Student;
import app.domain.Teacher;
import lombok.SneakyThrows;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.Constants;
import org.springframework.stereotype.Service;
import app.persistence.SchoolClassRepository;
import app.persistence.StudentRepository;
import app.persistence.TeacherRepository;
import org.springframework.transaction.TransactionSystemException;

import javax.persistence.RollbackException;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public record DatabaseInitializer(StudentRepository studentRepository,
                                  TeacherRepository teacherRepository,
                                  SchoolClassRepository schoolClassRepository) implements CommandLineRunner {

    @Override
    public void run(String... args) {
        readCSV();
        setupDatabase();
    }

    @SneakyThrows
    private void readCSV() {
        var fileName = "teachers.csv";
        var path = Path.of(getClass().getResource("/" + fileName).toURI());

        try (var lines = Files.lines(path)) {
            var logger = Logger.getLogger(getClass().getSimpleName());

            lines.map(line -> line.split(";"))
                    .filter(line -> line.length == 3)
                    .map(line -> new Teacher(line[0], line[1], line[2]))
                    .forEach(teacher -> {
                        try {
                            teacherRepository.save(teacher);
                        } catch (ConstraintViolationException | TransactionSystemException e) {
                            logger.info("Following teacher is invalid: " + teacher);
                        }
                    });
        }
    }

    private void setupDatabase() {
        var students = saveStudents();
        var teachers = saveTeachers();
        var schoolClasses = saveSchoolClasses(students, teachers);
    }

    private List<SchoolClass> saveSchoolClasses(List<Student> students, List<Teacher> teachers) {
        var schoolClasses = List.of(
                new SchoolClass("5AHIF"),
                new SchoolClass("5BHIF"),
                new SchoolClass("5CHIF")
        );

        schoolClasses.get(0).setStudents(students);
        schoolClasses.get(0).setTeachers(teachers);

        schoolClasses = schoolClassRepository.saveAll(schoolClasses);
        studentRepository.saveAll(students);
        teacherRepository.saveAll(teachers);
        return schoolClasses;
    }

    private List<Teacher> saveTeachers() {
        var teachers = List.of(
                new Teacher("scre", "Christoph", "Schreiber"),
                new Teacher("jeke", "Ina", "Jekel"),
                new Teacher("weix", "Martin", "Weixlbaum")
        );
        return teacherRepository.saveAll(teachers);
    }

    private List<Student> saveStudents() {
        var students = List.of(
                new Student(20170001, "Silvia", "Silberbauer", LocalDate.of(2000, 1, 1)),
                new Student(20170002, "Tobias", "Nolz", LocalDate.of(2000, 12, 12)),
                new Student(20170003, "Michael", "Gail", LocalDate.of(2000, 3, 13)),
                new Student(20170004, "Benedikt", "Schrabauer", LocalDate.of(2000, 11, 16)),
                new Student(20170005, "Alena", "Liebhard", LocalDate.of(2000, 8, 20))
        );
        return studentRepository.saveAll(students);
    }
}
