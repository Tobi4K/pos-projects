package app.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import app.domain.Student;
import app.domain.Teacher;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface TeacherRepository extends JpaRepository<Teacher, String> {

    @Query("""
            select distinct teacher
            from Student student
                join student.schoolClass schoolClass
                join schoolClass.teachers teacher
            where student = :student
            """)
    List<Teacher> findAllTeachersForStudent(Student student);

    @Query("""
            select distinct teacher
            from Student student, Teacher teacher, SchoolClass schoolClass
            where student = :student
                and student.schoolClass = schoolClass
                and teacher member of schoolClass.teachers
            """)
    List<Teacher> findAllTeachersForStudentWithoutJoin(Student student);
}
