package app.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import app.domain.SchoolClass;
import org.springframework.stereotype.Repository;

@Repository
public interface SchoolClassRepository extends JpaRepository<SchoolClass, String> {
}
