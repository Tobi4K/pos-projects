package app.persistence;

import app.domain.SchoolClass;
import org.springframework.data.jpa.repository.JpaRepository;
import app.domain.Student;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {

    List<Student> findAllBySchoolClass(SchoolClass schoolClass);
}
