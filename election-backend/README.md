# Election Backend
Das backend ist geschrieben in Kotlin (Spring). 
Eigentlich war ein dazu noch ein Frontend geplant, da das Backend allerdings schon ca. 10h Aufwand war, 
hat dies dann doch den Rahmen gesprengt.

### Anmerkungen
- Class coverage 91%, line coverage 95% (alles was sinnvoll für mich erscheint)
  - alle Service Klassen sind getestet
  - alle Endpoints sind getestet
- Die Start/End Funktion für die Election hätte folgenden Sinn gehabt
  - wenn keine Election => Personen können hinzugefügt weren, aber keine Punkte (entries)
  - wenn Election gestartet => Personen können nicht hinzugefügt werden, aber Votes können abgegeben werden
  - wenn Election beendet wird => Ausgabe des Ergebnisses und löschen der Daten für neue Election