package at.tobiasnolz.electionbackend.presentation.api

import at.tobiasnolz.electionbackend.domain.CandidateStanding
import at.tobiasnolz.electionbackend.domain.Entry
import at.tobiasnolz.electionbackend.interfaces.LogSubscriber
import at.tobiasnolz.electionbackend.service.ElectionService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid

@RestController
class ElectionControllerImpl(
    private val electionService: ElectionService,
    private val httpServletRequest: HttpServletRequest
) : ElectionController {

    val subscribers: MutableCollection<LogSubscriber> = HashSet()

    override fun startElection(): String {
        electionService.startElection().also {
            this.notifySubscribers(
                uri = getRequestURL(),
                description = "Election started"
            )
        }


        return "Election started"
    }

    override fun endElection(): Collection<CandidateStanding> {
        val standings = electionService.getStandings()
        electionService.endElection()

        this.notifySubscribers(
            uri = getRequestURL(),
            description = "Election ended and all data cleaned"
        )
        return standings
    }

    override fun getStandings(): Collection<CandidateStanding> {
        val standings = electionService.getStandings()
        this.notifySubscribers(
            uri = getRequestURL(),
            description = "GET for standings"
        )
        return standings
    }

    override fun getEntryById(@PathVariable(value = "id") entryId: Int): Entry {
        val entry = electionService.getEntryById(entryId)
        this.notifySubscribers(
            uri = getRequestURL(),
            description = "GET for entry with id \"$entryId\""
        )
        return entry
    }

    override fun saveEntry(@RequestBody @Valid entry: Entry): ResponseEntity<Entry> {
        val saved = electionService.save(entry)
        val uri = ServletUriComponentsBuilder
            .fromCurrentRequest()
            .path("/{id}")
            .build(saved.id)

        this.notifySubscribers(
            uri = getRequestURL(),
            description = "Saved new entry",
            data = saved
        )

        return ResponseEntity.created(uri)
            .body(saved)
    }

    override fun deleteEntry(entryId: Int) {
        electionService.deleteEntryById(entryId)

        this.notifySubscribers(
            uri = getRequestURL(),
            description = "Deleted entry with id \"$entryId\""
        )
    }

    override fun updateEntry(entryId: Int, entry: Entry): ResponseEntity<Entry> {
        val saved = electionService.update(entryId, entry)
        return if (saved.id == entryId) {
            this.notifySubscribers(
                uri = getRequestURL(),
                description = "Updated entry with id \"$entryId\"",
                data = saved
            )

            ResponseEntity.ok(saved)
        } else {
            this.notifySubscribers(
                uri = getRequestURL(),
                description = "Saved new entry",
                data = saved
            )

            val uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .build(saved.id)

            ResponseEntity.created(uri)
                .body(saved)
        }
    }

    override fun subscribe(subscriber: LogSubscriber) {
        this.subscribers.add(subscriber)
    }

    override fun unsubscribe(subscriber: LogSubscriber) {
        this.subscribers.remove(subscriber)
    }

    override fun notifySubscribers(uri: String, description: String, data: Any?) {
        this.subscribers.forEach { it.update(uri, description, data) }
    }

    private fun getRequestURL(): String {
        return httpServletRequest.requestURL.toString()
    }
}