package at.tobiasnolz.electionbackend.presentation.api

import at.tobiasnolz.electionbackend.domain.exception.LoggerException
import at.tobiasnolz.electionbackend.interfaces.LogSubscriber
import at.tobiasnolz.electionbackend.interfaces.Logger
import at.tobiasnolz.electionbackend.service.FileLoggerImpl
import org.springframework.web.bind.annotation.RestController

@RestController
class LoggerControllerImpl(
    private val personController: PersonController,
    private val electionController: ElectionController
) : LoggerController {

    private var logger: LogSubscriber? = null

    override fun enable(fileName: String): String {
        if (logger != null)
            throw LoggerException("Logger already active")

        logger = FileLoggerImpl(fileName = fileName)
        personController.subscribe(logger!!)
        electionController.subscribe(logger!!)
        return "Logging enabled (file: $fileName)"
    }

    override fun disable(): String {
        if (logger == null)
            throw LoggerException("No logger currently active")

        personController.unsubscribe(logger!!)
        electionController.unsubscribe(logger!!)
        logger = null

        return "Logging disabled"
    }
}