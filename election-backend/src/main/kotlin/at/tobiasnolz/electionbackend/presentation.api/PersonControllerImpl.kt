package at.tobiasnolz.electionbackend.presentation.api

import at.tobiasnolz.electionbackend.domain.Person
import at.tobiasnolz.electionbackend.interfaces.LogSubscriber
import at.tobiasnolz.electionbackend.service.ElectionService
import at.tobiasnolz.electionbackend.service.PersonService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid
import kotlin.collections.HashSet


@RestController
class PersonControllerImpl(
    private val personService: PersonService,
    private val electionService: ElectionService,
    private val httpServletRequest: HttpServletRequest
) : PersonController {

    val subscribers: MutableCollection<LogSubscriber> = HashSet()

    override fun save(@RequestBody @Valid person: Person): ResponseEntity<Person> {
        electionService.checkNotInProgressOrThrow()
        val saved = personService.save(person)
        val uri = ServletUriComponentsBuilder
            .fromCurrentRequest()
            .path("/{id}")
            .build(saved.id)

        this.notifySubscribers(
            uri = getRequestURL(),
            description = "New person saved",
            data = saved
        )

        return ResponseEntity.created(uri)
            .body(saved)
    }

    override fun getAll(): Collection<Person> {
        this.notifySubscribers(
            uri = getRequestURL(),
            description = "GET for all persons"
        )

        return personService.getAll()
    }

    override fun getPersonById(@PathVariable("id") personId: Int): Person {
        val person = personService.getById(personId)

        this.notifySubscribers(
            uri = getRequestURL(),
            description = "GET for person with id \"$personId\""
        )

        return person
    }

    override fun deletePerson(personId: Int) {
        electionService.checkNotInProgressOrThrow()
        personService.deletePersonById(personId)

        this.notifySubscribers(
            uri = getRequestURL(),
            description = "DELETED person with id \"$personId\""
        )
    }

    override fun subscribe(subscriber: LogSubscriber) {
        this.subscribers.add(subscriber)
    }

    override fun unsubscribe(subscriber: LogSubscriber) {
        this.subscribers.remove(subscriber)
    }

    override fun notifySubscribers(uri: String, description: String, data: Any?) {
        this.subscribers.forEach { it.update(uri, description, data) }
    }

    private fun getRequestURL(): String {
        return httpServletRequest.requestURL.toString()
    }

}