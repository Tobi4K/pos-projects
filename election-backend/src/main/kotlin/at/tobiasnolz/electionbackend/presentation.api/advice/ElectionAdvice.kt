package at.tobiasnolz.electionbackend.presentation.api.advice

import at.tobiasnolz.electionbackend.domain.exception.ElectionException
import org.springframework.core.annotation.Order
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice

@RestControllerAdvice
@Order(1)
class ElectionAdvice {
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ElectionException::class)
    fun handleElectionException(e: ElectionException): String? {
        return e.message
    }
}