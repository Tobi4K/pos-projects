package at.tobiasnolz.electionbackend.presentation.api

import at.tobiasnolz.electionbackend.domain.CandidateStanding
import at.tobiasnolz.electionbackend.domain.Entry
import at.tobiasnolz.electionbackend.interfaces.LogPublisher
import at.tobiasnolz.electionbackend.interfaces.LogSubscriber
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RequestMapping("api/election")
interface ElectionController: LogPublisher<LogSubscriber> {

    @GetMapping("/start")
    fun startElection(): String

    @GetMapping("/end")
    fun endElection(): Collection<CandidateStanding>

    @GetMapping("/standings")
    fun getStandings(): Collection<CandidateStanding>

    @GetMapping("/entry/{id}")
    fun getEntryById(@PathVariable("id") entryId: Int): Entry

    @PostMapping("/entry")
    fun saveEntry(@RequestBody @Valid entry: Entry): ResponseEntity<Entry>

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/entry/{id}")
    fun deleteEntry(@PathVariable("id") entryId: Int)

    @PutMapping("/entry/{id}")
    fun updateEntry(@PathVariable("id") entryId: Int, @RequestBody @Valid entry: Entry): ResponseEntity<Entry>
}