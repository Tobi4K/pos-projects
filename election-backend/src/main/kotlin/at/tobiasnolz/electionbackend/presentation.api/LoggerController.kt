package at.tobiasnolz.electionbackend.presentation.api

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping

@RequestMapping("api/log")
interface LoggerController {
    @GetMapping("/enable/{fileName}")
    fun enable(@PathVariable("fileName") fileName: String): String

    @GetMapping("/disable")
    fun disable(): String
}