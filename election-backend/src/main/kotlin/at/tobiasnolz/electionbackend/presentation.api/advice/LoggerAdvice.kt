package at.tobiasnolz.electionbackend.presentation.api.advice

import at.tobiasnolz.electionbackend.domain.exception.LoggerException
import org.springframework.core.annotation.Order
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice

@RestControllerAdvice
@Order(0)
class LoggerAdvice {
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(LoggerException::class)
    fun handleLoggerException(e: LoggerException): String? {
        return e.message
    }
}