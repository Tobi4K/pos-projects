package at.tobiasnolz.electionbackend.presentation.api.advice

import org.springframework.core.annotation.Order
import org.springframework.web.bind.annotation.RestControllerAdvice

@RestControllerAdvice
@Order(2)
class PersonAdvice