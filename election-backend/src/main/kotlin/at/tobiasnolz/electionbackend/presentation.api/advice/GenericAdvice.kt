package at.tobiasnolz.electionbackend.presentation.api.advice

import at.tobiasnolz.electionbackend.domain.exception.NoSuchEntryException
import at.tobiasnolz.electionbackend.domain.exception.NoSuchPersonException
import org.springframework.core.annotation.Order
import org.springframework.http.HttpStatus
import org.springframework.orm.jpa.JpaSystemException
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException


@RestControllerAdvice
@Order(3)
class GenericAdvice {
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(
        MethodArgumentTypeMismatchException::class,
        MethodArgumentNotValidException::class
    )
    fun handleMethodArgumentTypeMismatchException(e: Exception): String? {
        return "parameter(s) not in valid format"
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(
        IllegalArgumentException::class,
        JpaSystemException::class,
    )
    fun handleExceptions(e: Exception): String? {
        return e.message
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Throwable::class)
    fun handleThrowable(t: Throwable): String? {
        System.err.println(t.message)
        return "An error occured"
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoSuchPersonException::class, NoSuchEntryException::class)
    fun handleNoSuchElementExceptions(e: Exception): String? {
        return e.message
    }
}