package at.tobiasnolz.electionbackend.presentation.api

import at.tobiasnolz.electionbackend.domain.Person
import at.tobiasnolz.electionbackend.interfaces.LogPublisher
import at.tobiasnolz.electionbackend.interfaces.LogSubscriber
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RequestMapping("api/person")
interface PersonController: LogPublisher<LogSubscriber> {
    @PostMapping("")
    fun save(@RequestBody @Valid person: Person): ResponseEntity<Person>

    @GetMapping("")
    fun getAll(): Collection<Person>

    @GetMapping("/{id}")
    fun getPersonById(@PathVariable("id") personId: Int): Person

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    fun deletePerson(@PathVariable("id") personId: Int)
}