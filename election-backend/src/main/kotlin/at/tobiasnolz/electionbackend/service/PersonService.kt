package at.tobiasnolz.electionbackend.service

import at.tobiasnolz.electionbackend.domain.Person
import at.tobiasnolz.electionbackend.domain.exception.NoSuchPersonException
import at.tobiasnolz.electionbackend.persistence.PersonRepository
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.stereotype.Service

@Service
class PersonService(private val personRepository: PersonRepository) {

    fun save(person: Person): Person {
        return personRepository.save(person)
    }

    fun getAll(): Collection<Person> {
        return personRepository.findAll()
    }

    fun getById(id: Int): Person {
        return personRepository.findById(id)
            .orElseThrow { NoSuchPersonException(id) }
    }

    fun deletePersonById(id: Int) {
        try {
            personRepository.deleteById(id)
        } catch (e: EmptyResultDataAccessException) {
            throw NoSuchPersonException(id)
        }
    }
}