package at.tobiasnolz.electionbackend.service

import at.tobiasnolz.electionbackend.consts.PRIMARY_VOTE_POINTS
import at.tobiasnolz.electionbackend.consts.SECONDARY_VOTE_POINTS
import at.tobiasnolz.electionbackend.domain.CandidateStanding
import at.tobiasnolz.electionbackend.domain.Entry
import at.tobiasnolz.electionbackend.domain.exception.ElectionException
import at.tobiasnolz.electionbackend.domain.exception.NoSuchEntryException
import at.tobiasnolz.electionbackend.domain.exception.NoSuchPersonException
import at.tobiasnolz.electionbackend.persistence.EntryRepository
import at.tobiasnolz.electionbackend.persistence.PersonRepository
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.stereotype.Service

@Service
class ElectionService(
    private val entryRepository: EntryRepository,
    private val personRepository: PersonRepository
) {

    private var electionInProgress = false

    fun startElection() {
        checkNotInProgressOrThrow()
        this.electionInProgress = true
    }

    fun endElection() {
        checkInProgressOrThrow()
        entryRepository.deleteAll()
        personRepository.deleteAll()
        this.electionInProgress = false
    }

    fun checkInProgressOrThrow() {
        if (!this.electionInProgress)
            throw ElectionException("No election currently in progress")
    }

    fun checkNotInProgressOrThrow() {
        if (this.electionInProgress)
            throw ElectionException("Election is already in progress")
    }

    fun save(entry: Entry): Entry {
        checkInProgressOrThrow()

        if (entry.secondaryCandidateId != null) {
            checkCandidatesEqual(entry)

            personRepository.findById(entry.secondaryCandidateId!!)
                .orElseThrow { throw NoSuchPersonException(entry.secondaryCandidateId!!) }
        }

        personRepository.findById(entry.primaryCandidateId)
            .orElseThrow { throw NoSuchPersonException(entry.primaryCandidateId) }

        return entryRepository.save(entry)
    }

    private fun checkCandidatesEqual(entry: Entry) {
        if (entry.secondaryCandidateId == entry.primaryCandidateId)
            throw ElectionException("Primary candidate & secondary candidate may not be the same")
    }

    fun update(id: Int, entry: Entry): Entry {
        checkInProgressOrThrow()
        val toSave = entryRepository.findById(id).map { orgEntry ->
            orgEntry.primaryCandidateId = entry.primaryCandidateId
            orgEntry.secondaryCandidateId = entry.secondaryCandidateId
            this.save(orgEntry)
        }.orElse(
            entry
        )
        return this.save(toSave)
    }

    fun getEntryById(id: Int): Entry {
        checkInProgressOrThrow()
        return entryRepository.findById(id)
            .orElseThrow { NoSuchEntryException(id) }
    }

    fun deleteEntryById(id: Int) {
        checkInProgressOrThrow()
        try {
            entryRepository.deleteById(id)
        } catch (e: EmptyResultDataAccessException) {
            throw NoSuchEntryException(id)
        }
    }

    fun getStandings(): List<CandidateStanding> {
        checkInProgressOrThrow()
        val persons = personRepository.findAll()

        return persons.map { person ->
            val primaryVotes: Int = entryRepository.countAllByPrimaryCandidateId(person.id)
            val secondaryVotes = entryRepository.countAllBySecondaryCandidateId(person.id)
            val points = primaryVotes * PRIMARY_VOTE_POINTS + secondaryVotes * SECONDARY_VOTE_POINTS

            CandidateStanding(
                person = person,
                primaryVotes = primaryVotes,
                secondaryVotes = secondaryVotes,
                points = points
            )
        }.toList()
    }
}