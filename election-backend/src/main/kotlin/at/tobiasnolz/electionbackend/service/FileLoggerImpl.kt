package at.tobiasnolz.electionbackend.service

import at.tobiasnolz.electionbackend.interfaces.LogSubscriber
import at.tobiasnolz.electionbackend.interfaces.Logger
import java.io.FileOutputStream
import java.time.Instant
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

class FileLoggerImpl(private val fileName: String) : LogSubscriber, Logger {

    override fun update(uri: String, description: String, data: Any?) {
        log(uri, description, data)
    }

    override fun log(uri: String, description: String, data: Any?) {
        val logOutput = StringBuilder()
        logOutput.appendLine("${getTimestamp()} URI executed: $uri")
        logOutput.appendLine("${getTimestamp()} INFO: $description")
        if (data != null)
            logOutput.appendLine("${getTimestamp()} DATA: $data")

        FileOutputStream(fileName, true).bufferedWriter().use { writer ->
            writer.write(logOutput.toString())
        }
    }

    private fun getTimestamp(): String {
        return DateTimeFormatter
            .ofPattern("yyyy-MM-dd HH:mm:ss.SSS")
            .withZone(ZoneOffset.UTC)
            .format(Instant.now()).toString()
    }

}