package at.tobiasnolz.electionbackend.domain

data class CandidateStanding(
    val person: Person,
    val points: Int,
    val primaryVotes: Int,
    val secondaryVotes: Int
)