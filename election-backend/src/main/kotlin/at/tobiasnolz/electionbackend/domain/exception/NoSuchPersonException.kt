package at.tobiasnolz.electionbackend.domain.exception

class NoSuchPersonException(id: Int) :
    RuntimeException("Person with id $id not found")