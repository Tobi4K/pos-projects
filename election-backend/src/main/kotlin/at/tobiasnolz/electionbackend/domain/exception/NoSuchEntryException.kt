package at.tobiasnolz.electionbackend.domain.exception

class NoSuchEntryException(id: Int) :
    RuntimeException("No entry with id \"$id\" found")