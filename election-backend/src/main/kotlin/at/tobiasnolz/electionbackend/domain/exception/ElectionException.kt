package at.tobiasnolz.electionbackend.domain.exception

class ElectionException(message: String) :
RuntimeException(message)

