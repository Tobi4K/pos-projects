package at.tobiasnolz.electionbackend.domain

import javax.persistence.*

@Entity
class Entry(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int = 0,

    var primaryCandidateId: Int,

    var secondaryCandidateId: Int? = null

    /*
    Problem: ich möchte nicht jedes mal das ganze Person Objekt mitschicken
     -> manuelles serializen/deserializen nicht nachvollziehbar für mich

    @NotNull
    @ManyToOne(optional = false)
    var primaryCandidate: Person,

    @ManyToOne(optional = true)
    var secondaryCandidate: Person?
     */
) {
    override fun toString(): String {
        return "Entry(id=$id, primaryCandidateId=$primaryCandidateId, secondaryCandidateId=$secondaryCandidateId)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Entry

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id
    }
}