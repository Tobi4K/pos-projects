package at.tobiasnolz.electionbackend.domain.exception

class LoggerException(message: String) :
    RuntimeException(message)

