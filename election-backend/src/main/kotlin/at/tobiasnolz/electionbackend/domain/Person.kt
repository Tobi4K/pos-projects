package at.tobiasnolz.electionbackend.domain

import javax.persistence.*
import javax.validation.constraints.NotBlank

@Entity
//@Table(uniqueConstraints = [
//    UniqueConstraint(columnNames = ["firstName", "lastName"])
//])
// --> Namensgleichheit.....
class Person(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int = 0,

    @field:NotBlank
    val lastName: String,

    @field:NotBlank
    val firstName: String

    ) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Person

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id
    }

    override fun toString(): String {
        return "Person(id=$id, lastName='$lastName', firstName='$firstName')"
    }

}