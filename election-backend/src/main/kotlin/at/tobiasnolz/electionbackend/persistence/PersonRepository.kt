package at.tobiasnolz.electionbackend.persistence

import at.tobiasnolz.electionbackend.domain.Person
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface PersonRepository: JpaRepository<Person, Int>