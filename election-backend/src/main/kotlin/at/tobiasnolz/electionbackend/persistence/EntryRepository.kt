package at.tobiasnolz.electionbackend.persistence

import at.tobiasnolz.electionbackend.domain.Entry
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface EntryRepository: JpaRepository<Entry, Int> {
    fun countAllByPrimaryCandidateId(id: Int): Int
    fun countAllBySecondaryCandidateId(id: Int): Int
}