package at.tobiasnolz.electionbackend.interfaces

interface Logger {
    fun log(uri: String, description: String, data: Any?)
}