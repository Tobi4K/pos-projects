package at.tobiasnolz.electionbackend.interfaces

interface LogSubscriber {
    fun update(uri: String, description: String, data: Any?)
}