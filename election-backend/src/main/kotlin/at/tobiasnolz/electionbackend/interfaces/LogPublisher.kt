package at.tobiasnolz.electionbackend.interfaces

interface LogPublisher<T> {
    fun subscribe(subscriber: T)
    fun unsubscribe(subscriber: T)
    fun notifySubscribers(uri: String, description: String, data: Any? = null)
}