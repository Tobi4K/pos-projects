package at.tobiasnolz.electionbackend

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ElectionBackendApplication

fun main(args: Array<String>) {
    runApplication<ElectionBackendApplication>(*args)
}
