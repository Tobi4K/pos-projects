package at.tobiasnolz.electionbackend.consts

const val PRIMARY_VOTE_POINTS = 2
const val SECONDARY_VOTE_POINTS = 1