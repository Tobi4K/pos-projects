package at.tobiasnolz.electionbackend.service

import at.tobiasnolz.electionbackend.domain.Person
import at.tobiasnolz.electionbackend.domain.exception.NoSuchPersonException
import at.tobiasnolz.electionbackend.persistence.PersonRepository
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@AutoConfigureMockMvc
internal class PersonServiceTest(
    @Autowired private val personRepository: PersonRepository
) {

    private val personService: PersonService = PersonService(personRepository)

    @BeforeEach
    internal fun setUp() {
        val persons = listOf(
            Person(firstName = "Tobias", lastName = "Nolz"),
            Person(firstName = "Michael", lastName = "Gail")
        )
        personRepository.saveAll(persons)
    }

    @AfterEach
    internal fun cleanup() {
        personRepository.deleteAll()
    }

    @Test
    fun save_success() {
        val toSave = Person(firstName = "Max", lastName = "Mustermann")
        val saved = this.personService.save(toSave)
        val persisted = this.personRepository.findById(saved.id)
            .orElseThrow { throw RuntimeException("Person service \"save\" function failed") }

        assertEquals(toSave, saved)
        assertEquals(toSave, persisted)
        assertEquals(toSave.lastName, persisted.lastName)
        assertEquals(toSave.firstName, persisted.firstName)
    }

    @Test
    fun save_lastNameBlank_expectException() {
        val toSave = Person(firstName = "Test", lastName = "")
        assertThrows<RuntimeException> { this.personService.save(toSave) }
    }

    @Test
    fun save_firstNameBlank_expectException() {
        val toSave = Person(firstName = "", lastName = "Mustermann")
        assertThrows<RuntimeException> { this.personService.save(toSave) }
    }

    @Test
    fun getAll_success() {
        val expected = this.personRepository.findAll()
        val result = this.personService.getAll()
        assertEquals(expected, result)
    }

    @Test
    fun getById_success() {
        val entries = this.personRepository.findAll()

        val expected = entries[0]
        val result = this.personRepository.getById(expected.id)

        assertEquals(expected, result)
        assertEquals(expected.firstName, result.firstName)
        assertEquals(expected.lastName, result.lastName)
    }

    @Test
    fun getById_invalidId_expectException() {
        assertThrows<NoSuchPersonException> { this.personService.getById(9999) }
    }

    @Test
    fun deletePerson_success() {
        val persons = this.personRepository.findAll()

        val toDelete = persons[0]
        this.personService.deletePersonById(toDelete.id)

        val result = this.personRepository.findAll()
        assertFalse(result.contains(toDelete))
    }

    @Test
    fun delete_invalidId_expectException() {
        assertThrows<NoSuchPersonException> { this.personService.deletePersonById(9999) }
    }
}