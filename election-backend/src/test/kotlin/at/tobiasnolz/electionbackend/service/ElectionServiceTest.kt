package at.tobiasnolz.electionbackend.service

import at.tobiasnolz.electionbackend.consts.PRIMARY_VOTE_POINTS
import at.tobiasnolz.electionbackend.consts.SECONDARY_VOTE_POINTS
import at.tobiasnolz.electionbackend.domain.Entry
import at.tobiasnolz.electionbackend.domain.Person
import at.tobiasnolz.electionbackend.domain.exception.ElectionException
import at.tobiasnolz.electionbackend.domain.exception.NoSuchEntryException
import at.tobiasnolz.electionbackend.domain.exception.NoSuchPersonException
import at.tobiasnolz.electionbackend.persistence.EntryRepository
import at.tobiasnolz.electionbackend.persistence.PersonRepository
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@AutoConfigureMockMvc
class ElectionServiceTest(
    @Autowired private val entryRepository: EntryRepository,
    @Autowired private val personRepository: PersonRepository
) {
    private val electionService: ElectionService = ElectionService(entryRepository, personRepository)

    @BeforeEach
    internal fun setUp() {
        var persons = listOf(
            Person(firstName = "Tobias", lastName = "Nolz"),
            Person(firstName = "Michael", lastName = "Gail")
        )
        persons = personRepository.saveAll(persons)

        val entries = listOf(
            Entry(primaryCandidateId = persons[0].id, secondaryCandidateId = persons[1].id),
            Entry(primaryCandidateId = persons[0].id, secondaryCandidateId = persons[1].id),
            Entry(primaryCandidateId = persons[0].id, secondaryCandidateId = persons[1].id),
            Entry(primaryCandidateId = persons[1].id)
        )
        entryRepository.saveAll(entries)
        this.electionService.startElection()
    }

    @AfterEach
    internal fun cleanup() {
        personRepository.deleteAll()
        entryRepository.deleteAll()
    }

    @Test
    fun endElection_success() {
        this.electionService.endElection()
        assertTrue(this.entryRepository.findAll().isEmpty())
        assertTrue(this.personRepository.findAll().isEmpty())
    }

    @Test
    fun checkInProgressOrThrow_expectException() {
        this.electionService.endElection()
        assertThrows<ElectionException> {
            this.electionService.checkInProgressOrThrow()
        }
    }

    @Test
    fun checkInProgressOrThrow_success() {
        assertDoesNotThrow { this.electionService.checkInProgressOrThrow() }
    }

    @Test
    fun checkNotInProgressOrThrow_expectException() {
        assertThrows<ElectionException> {
            this.electionService.checkNotInProgressOrThrow()
        }
    }

    @Test
    fun checkNotInProgressOrThrow_success() {
        this.electionService.endElection()
        assertDoesNotThrow { this.electionService.checkNotInProgressOrThrow() }
    }

    @Test
    fun save_success() {
        val persons = this.personRepository.findAll()

        val toSave = Entry(primaryCandidateId = persons[0].id, secondaryCandidateId = persons[1].id)
        val saved = this.electionService.save(toSave)
        val lastId = entryRepository.findAll().last().id

        val persisted = entryRepository.findById(lastId).orElseThrow {
            throw RuntimeException("Election service \"save\" function failed")
        }

        assertEquals(toSave, saved)
        assertEquals(toSave, persisted)
    }

    @Test
    fun save_success_secondaryMissing() {
        val persons = this.personRepository.findAll()

        val toSave = Entry(primaryCandidateId = persons[0].id)
        val saved = this.electionService.save(toSave)

        val persisted = entryRepository.findById(saved.id).orElseThrow {
            throw RuntimeException("Election service \"save\" function failed")
        }

        assertEquals(toSave, saved)
        assertEquals(toSave, persisted)
    }

    @Test
    fun save_fail_candidatesEqual() {
        val persons = this.personRepository.findAll()

        val toSave = Entry(primaryCandidateId = persons[0].id, secondaryCandidateId = persons[0].id)
        assertThrows<ElectionException> { this.electionService.save(toSave) }
    }

    @Test
    fun save_fail_primaryVoteIdNotValid() {
        val persons = this.personRepository.findAll()

        val toSave = Entry(primaryCandidateId = 9999, secondaryCandidateId = persons[0].id)
        assertThrows<NoSuchPersonException> { this.electionService.save(toSave) }
    }

    @Test
    fun save_fail_secondaryVoteIdNotValid() {
        val persons = this.personRepository.findAll()

        val toSave = Entry(primaryCandidateId = persons[0].id, secondaryCandidateId = 9999)
        assertThrows<NoSuchPersonException> { this.electionService.save(toSave) }
    }

    @Test
    fun update_updateExisting_success() {
        val entries = this.entryRepository.findAll()
        val persons = this.personRepository.findAll()
        val entryToModifyId = entries[0].id

        val toUpdate = Entry(primaryCandidateId = persons[1].id, secondaryCandidateId = persons[0].id)
        val saved = this.electionService.update(entryToModifyId, toUpdate)

        assertEquals(entryToModifyId, saved.id)
        assertEquals(toUpdate.primaryCandidateId, saved.primaryCandidateId)
        assertEquals(toUpdate.secondaryCandidateId, saved.secondaryCandidateId)
    }

    @Test
    fun update_updateNonExisting_success() {
        val persons = this.personRepository.findAll()
        val entryToModifyId = 9999

        val toUpdate = Entry(primaryCandidateId = persons[1].id, secondaryCandidateId = persons[0].id)
        val saved = this.electionService.update(entryToModifyId, toUpdate)
        val allEntries = this.entryRepository.findAll()

        assertEquals(allEntries.last().id, saved.id)
        assertEquals(toUpdate.primaryCandidateId, saved.primaryCandidateId)
        assertEquals(toUpdate.secondaryCandidateId, saved.secondaryCandidateId)
    }

    @Test
    fun getEntryById_success() {
        val entries = this.entryRepository.findAll()

        val expected = entries[0]
        val result = this.electionService.getEntryById(expected.id)

        assertEquals(expected, result)
        assertEquals(expected.primaryCandidateId, result.primaryCandidateId)
        assertEquals(expected.secondaryCandidateId, result.secondaryCandidateId)
    }

    @Test
    fun getEntryById_getNonExisting_expectException() {
        assertThrows<NoSuchEntryException> { this.electionService.getEntryById(9999) }
    }

    @Test
    fun deleteEntry_success() {
        val entries = this.entryRepository.findAll()

        val toDelete = entries[0]
        this.electionService.deleteEntryById(toDelete.id)

        val result = this.entryRepository.findAll()
        assertFalse(result.contains(toDelete))
    }

    @Test
    fun deleteEntry_deleteNonExisting_expectException() {
        assertThrows<NoSuchEntryException> { this.electionService.deleteEntryById(9999) }
    }

    @Test
    fun getStandings() {
        val persons = this.personRepository.findAll()
        val result = this.electionService.getStandings()

        persons.forEachIndexed { index, person ->
            val primaryVotes: Int = entryRepository.countAllByPrimaryCandidateId(person.id)
            val secondaryVotes = entryRepository.countAllBySecondaryCandidateId(person.id)
            val points = primaryVotes * PRIMARY_VOTE_POINTS + secondaryVotes * SECONDARY_VOTE_POINTS

            assertEquals(person, result[index].person)
            assertEquals(primaryVotes, result[index].primaryVotes)
            assertEquals(secondaryVotes, result[index].secondaryVotes)
            assertEquals(points, result[index].points)
        }

    }
}