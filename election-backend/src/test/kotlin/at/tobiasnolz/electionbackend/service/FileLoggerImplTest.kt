package at.tobiasnolz.electionbackend.service

import at.tobiasnolz.electionbackend.interfaces.LogPublisher
import at.tobiasnolz.electionbackend.interfaces.LogSubscriber
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import java.io.File

private const val FILE_NAME = "loggerTest.txt"

@SpringBootTest
@AutoConfigureMockMvc
internal class FileLoggerImplTest {

    private val logger = FileLoggerImpl(FILE_NAME)
    private val tester = LogTest()

    @BeforeEach
    internal fun setUp() {
        tester.subscribe(logger)
    }

    @AfterEach
    internal fun tearDown() {
        tester.unsubscribe(logger)
        val file = File(FILE_NAME)
        if (file.exists())
            file.delete()
    }

    @Test
    fun update_success() {
        val uri = "https://test.com/test"
        val description = "Test Description"
        val data = "Test Data Object"

        logger.update(uri, description, data)
        val lines = File(FILE_NAME).readLines()

        if (lines.size != 3)
            throw RuntimeException("Log line count incorrect")

        assertTrue(lines[0].contains(uri))
        assertTrue(lines[1].contains(description))
        assertTrue(lines[2].contains(data))
    }
}

internal class LogTest : LogPublisher<LogSubscriber> {
    private val subscribers: MutableCollection<LogSubscriber> = HashSet()

    override fun subscribe(subscriber: LogSubscriber) {
        this.subscribers.add(subscriber)
    }

    override fun unsubscribe(subscriber: LogSubscriber) {
        this.subscribers.remove(subscriber)
    }

    override fun notifySubscribers(uri: String, description: String, data: Any?) {
        this.subscribers.forEach { it.update(uri, description, data) }
    }
}