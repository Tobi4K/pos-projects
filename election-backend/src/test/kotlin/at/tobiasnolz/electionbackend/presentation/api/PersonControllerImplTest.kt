package at.tobiasnolz.electionbackend.presentation.api

import at.tobiasnolz.electionbackend.domain.Entry
import at.tobiasnolz.electionbackend.domain.Person
import at.tobiasnolz.electionbackend.persistence.EntryRepository
import at.tobiasnolz.electionbackend.persistence.PersonRepository
import at.tobiasnolz.electionbackend.service.ElectionService
import org.hamcrest.CoreMatchers.containsStringIgnoringCase
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ExtendWith(SpringExtension::class)
internal class PersonControllerImplTest(
    @Autowired private val entryRepository: EntryRepository,
    @Autowired private val personRepository: PersonRepository,
) {

    @Autowired
    private lateinit var mvc: MockMvc

    private val electionService: ElectionService = ElectionService(entryRepository, personRepository)

    @BeforeEach
    internal fun setUp() {
        var persons = listOf(
            Person(firstName = "Tobias", lastName = "Nolz"),
            Person(firstName = "Michael", lastName = "Gail")
        )
        persons = personRepository.saveAll(persons)

        val entries = listOf(
            Entry(primaryCandidateId = persons[0].id, secondaryCandidateId = persons[1].id),
            Entry(primaryCandidateId = persons[0].id, secondaryCandidateId = persons[1].id),
            Entry(primaryCandidateId = persons[0].id, secondaryCandidateId = persons[1].id),
            Entry(primaryCandidateId = persons[1].id)
        )
        entryRepository.saveAll(entries)
        this.electionService.startElection()
    }

    @AfterEach
    internal fun cleanup() {
        personRepository.deleteAll()
        entryRepository.deleteAll()
    }

    @Test
    fun save() {
        val firstName = "Max"
        val lastName = "Mustermann"
        val json = getJsonUserRequest(firstName, lastName)

        val oldPersonList = personRepository.findAll()

        mvc.perform(post("/api/person")
            .contentType(MediaType.APPLICATION_JSON)
            .content(json)
            .characterEncoding("utf-8"))
            .andDo(print())
            .andExpect(status().isCreated)
            .andExpect(jsonPath("$.firstName").value(firstName))
            .andExpect(jsonPath("$.lastName").value(lastName))

        val result = personRepository.findAll()
        result.removeAll(oldPersonList)
        assertEquals(1, result.size)
        assertEquals(firstName, result[0].firstName)
        assertEquals(lastName, result[0].lastName)
    }

    @Test
    fun getAll() {
        val expected = this.personRepository.findAll()

        val result = mvc.perform(
            get("/api/person")
                .contentType(MediaType.APPLICATION_JSON)
        )
            .andDo(print())
            .andExpect(status().isOk)
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))

        for (personIterator in 0 until expected.size) {
            result.andExpect(jsonPath("$[$personIterator].firstName").value(expected[personIterator].firstName))
                .andExpect(jsonPath("$[$personIterator].lastName").value(expected[personIterator].lastName))
                .andExpect(jsonPath("$[$personIterator].id").value(expected[personIterator].id))
        }
    }

    @Test
    fun getPersonById_success() {
        val persons = this.personRepository.findAll()

        persons.forEach { expected ->
            mvc.perform(get("/api/person/${expected.id}").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.firstName").value(expected.firstName))
                .andExpect(jsonPath("$.lastName").value(expected.lastName))
                .andExpect(jsonPath("$.id").value(expected.id))
        }
    }

    @Test
    fun getPersonById_fail() {
        mvc.perform(get("/api/person/9999").contentType(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().isNotFound)
            .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN))
            .andExpect(content().string(containsStringIgnoringCase("9999")))
    }

    @Test
    fun deletePerson_success() {
        val persons = this.personRepository.findAll()

        persons.forEach { expected ->
            mvc.perform(delete("/api/person/${expected.id}").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNoContent)
        }

        val result = this.personRepository.findAll()
        assertTrue(result.isEmpty())
    }

    @Test
    fun deletePerson_fail() {
        mvc.perform(delete("/api/person/9999").contentType(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().isNotFound)
            .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN))
            .andExpect(content().string(containsStringIgnoringCase("9999")))
    }

    fun getJsonUserRequest(firstName: String, lastName: String): String {
        return """
            {
                "firstName": "$firstName",
                "lastName": "$lastName"
            }
        """.trimIndent()
    }
}
