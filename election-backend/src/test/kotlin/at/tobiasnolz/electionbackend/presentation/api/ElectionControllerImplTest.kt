package at.tobiasnolz.electionbackend.presentation.api

import at.tobiasnolz.electionbackend.consts.PRIMARY_VOTE_POINTS
import at.tobiasnolz.electionbackend.consts.SECONDARY_VOTE_POINTS
import at.tobiasnolz.electionbackend.domain.Entry
import at.tobiasnolz.electionbackend.domain.Person
import at.tobiasnolz.electionbackend.persistence.EntryRepository
import at.tobiasnolz.electionbackend.persistence.PersonRepository
import org.hamcrest.CoreMatchers.containsStringIgnoringCase
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.extension.ExtendWith

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ExtendWith(SpringExtension::class)
@TestMethodOrder(OrderAnnotation::class)
internal class ElectionControllerImplTest(
    @Autowired private val entryRepository: EntryRepository,
    @Autowired private val personRepository: PersonRepository,
) {

    @Autowired
    private lateinit var mvc: MockMvc

    @BeforeEach
    internal fun setUp() {
        var persons = listOf(
            Person(firstName = "Tobias", lastName = "Nolz"),
            Person(firstName = "Michael", lastName = "Gail")
        )
        persons = personRepository.saveAll(persons)

        val entries = listOf(
            Entry(primaryCandidateId = persons[0].id, secondaryCandidateId = persons[1].id),
            Entry(primaryCandidateId = persons[0].id, secondaryCandidateId = persons[1].id),
            Entry(primaryCandidateId = persons[0].id, secondaryCandidateId = persons[1].id),
            Entry(primaryCandidateId = persons[1].id)
        )
        entryRepository.saveAll(entries)
    }

    @AfterEach
    internal fun cleanup() {
        personRepository.deleteAll()
        entryRepository.deleteAll()
    }

    @Order(1)
    @Test
    fun startElection_success() {
        mvc.perform(get("/api/election/start").contentType(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().isOk)
            .andExpect(content().string(containsStringIgnoringCase("election started")))
    }

    @Order(2)
    @Test
    fun startElection_fail_alreadyStarted() {
        mvc.perform(get("/api/election/start").contentType(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().isBadRequest)
            .andExpect(content().string(containsStringIgnoringCase("election is already in progress")))
    }

    @Order(3)
    @Test
    fun endElection_success() {
        mvc.perform(get("/api/election/end").contentType(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().isOk)
    }

    @Order(4)
    @Test
    fun endElection_fail_notStarted() {
        mvc.perform(get("/api/election/end").contentType(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().isBadRequest)
            .andExpect(content().string(containsStringIgnoringCase("no election currently in progress")))
    }

    @Order(5)
    @Test
    fun getStandings() {
        this.startElection_success() // For further testing

        val result = mvc.perform(get("/api/election/standings").contentType(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().isOk)

        val persons = this.personRepository.findAll()

        persons.forEachIndexed { index, person ->
            val primaryVotes: Int = entryRepository.countAllByPrimaryCandidateId(persons[index].id)
            val secondaryVotes = entryRepository.countAllBySecondaryCandidateId(persons[index].id)
            val points = primaryVotes * PRIMARY_VOTE_POINTS + secondaryVotes * SECONDARY_VOTE_POINTS

            result.andExpect(jsonPath("$[$index].person.firstName").value(person.firstName))
                .andExpect(jsonPath("$[$index].person.lastName").value(person.lastName))
                .andExpect(jsonPath("$[$index].person.id").value(person.id))
                .andExpect(jsonPath("$[$index].primaryVotes").value(primaryVotes))
                .andExpect(jsonPath("$[$index].secondaryVotes").value(secondaryVotes))
                .andExpect(jsonPath("$[$index].points").value(points))
        }
    }

    @Order(6)
    @Test
    fun getEntryById_success() {
        val entries = this.entryRepository.findAll()

        entries.forEach { entry ->
            mvc.perform(get("/api/election/entry/${entry.id}").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.primaryCandidateId").value(entry.primaryCandidateId))
                .andExpect(jsonPath("$.secondaryCandidateId").value(entry.secondaryCandidateId))
                .andExpect(jsonPath("$.id").value(entry.id))
        }
    }

    @Order(7)
    @Test
    fun getEntryById_fail_invalidId() {
        mvc.perform(get("/api/election/entry/9999").contentType(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().isNotFound)
            .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN))
            .andExpect(content().string(containsStringIgnoringCase("9999")))
    }

    @Order(8)
    @Test
    fun saveEntry_success() {
        val persons = this.personRepository.findAll()
        val primaryCandidateId = persons[0].id
        val secondaryCandidateId = persons[1].id
        val json = getJsonEntryRequest(primaryCandidateId, secondaryCandidateId)

        val oldEntryList = entryRepository.findAll()

        mvc.perform(
            post("/api/election/entry")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .characterEncoding("utf-8")
        )
            .andDo(print())
            .andExpect(status().isCreated)
            .andExpect(jsonPath("$.primaryCandidateId").value(primaryCandidateId))
            .andExpect(jsonPath("$.secondaryCandidateId").value(secondaryCandidateId))

        val result = entryRepository.findAll()

        result.removeAll(oldEntryList)
        assertEquals(1, result.size)
        assertEquals(primaryCandidateId, result[0].primaryCandidateId)
        assertEquals(secondaryCandidateId, result[0].secondaryCandidateId)
    }

    @Order(9)
    @Test
    fun saveEntry_fail_duplicatePerson() {
        val persons = this.personRepository.findAll()
        val primaryCandidateId = persons[0].id
        val secondaryCandidateId = persons[0].id
        val json = getJsonEntryRequest(primaryCandidateId, secondaryCandidateId)

        val oldEntryList = entryRepository.findAll()

        mvc.perform(
            post("/api/election/entry")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .characterEncoding("utf-8")
        )
            .andDo(print())
            .andExpect(status().isBadRequest)
            .andExpect(content().string(containsStringIgnoringCase("same")))
    }

    @Order(10)
    @Test
    fun deleteEntry_success() {
        val entries = this.entryRepository.findAll()

        entries.forEach { entry ->
            mvc.perform(delete("/api/election/entry/${entry.id}").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNoContent)
        }

        val result = this.entryRepository.findAll()
        assertTrue(result.isEmpty())
    }

    @Order(11)
    @Test
    fun deleteEntry_fail() {
        mvc.perform(delete("/api/election/entry/9999").contentType(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().isNotFound)
    }

    @Order(12)
    @Test
    fun updateEntry_updated_success() {
        val entryToUpdate = this.entryRepository.findAll().first()
        val persons = this.personRepository.findAll()

        val primaryCandidateId = persons[1].id
        val secondaryCandidateId = null
        val json = getJsonEntryRequest(primaryCandidateId, secondaryCandidateId)

        mvc.perform(
            put("/api/election/entry/${entryToUpdate.id}")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .characterEncoding("utf-8")
        ).andDo(print())
            .andExpect(status().isOk)
            .andExpect(jsonPath("$.primaryCandidateId").value(primaryCandidateId))
            .andExpect(jsonPath("$.secondaryCandidateId").value(secondaryCandidateId))
            .andExpect(jsonPath("$.id").value(entryToUpdate.id))

        val result = this.entryRepository.findById(entryToUpdate.id)
            .orElseThrow { throw RuntimeException("Result should not be null") }

        assertEquals(entryToUpdate.id, result.id)
        assertEquals(primaryCandidateId, result.primaryCandidateId)
        assertEquals(secondaryCandidateId, result.secondaryCandidateId)
    }

    @Order(13)
    @Test
    fun updateEntry_created_success_() {
        val nonExistingId = 9999
        val newId = this.entryRepository.findAll().last().id + 1
        val persons = this.personRepository.findAll()

        val primaryCandidateId = persons[1].id
        val secondaryCandidateId = persons[0].id
        val json = getJsonEntryRequest(primaryCandidateId, secondaryCandidateId)

        mvc.perform(
            put("/api/election/entry/$nonExistingId")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .characterEncoding("utf-8")
        ).andDo(print())
            .andExpect(status().isCreated)
            .andExpect(jsonPath("$.primaryCandidateId").value(primaryCandidateId))
            .andExpect(jsonPath("$.secondaryCandidateId").value(secondaryCandidateId))
            .andExpect(jsonPath("$.id").value(newId))

        val result = this.entryRepository.findById(newId)
            .orElseThrow { throw RuntimeException("Result should not be null") }

        assertEquals(newId, result.id)
        assertEquals(primaryCandidateId, result.primaryCandidateId)
        assertEquals(secondaryCandidateId, result.secondaryCandidateId)
    }

    fun getJsonEntryRequest(primaryCandidateId: Int?, secondaryCandidateId: Int?): String {
        return """
            {
                "primaryCandidateId": $primaryCandidateId,
                "secondaryCandidateId": $secondaryCandidateId
            }
        """.trimIndent()
    }
}