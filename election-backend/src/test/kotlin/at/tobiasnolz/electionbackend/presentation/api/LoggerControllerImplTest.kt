package at.tobiasnolz.electionbackend.presentation.api

import org.hamcrest.CoreMatchers
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.TestMethodOrder
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ExtendWith(SpringExtension::class)
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
internal class LoggerControllerImplTest {

    @Autowired
    private lateinit var mvc: MockMvc

    @Order(1)
    @Test
    fun enable_success() {
        mvc.perform(get("/api/log/enable/log.txt").contentType(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().isOk)
            .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN))
            .andExpect(content().string(CoreMatchers.containsStringIgnoringCase("enabled")))
    }

    @Order(2)
    @Test
    fun enable_failDuplicate_expectException() {
        mvc.perform(get("/api/log/enable/log.txt").contentType(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().isBadRequest)
            .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN))
            .andExpect(content().string(CoreMatchers.containsStringIgnoringCase("already active")))
    }

    @Order(3)
    @Test
    fun disable_success() {
        mvc.perform(get("/api/log/disable").contentType(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().isOk)
            .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN))
            .andExpect(content().string(CoreMatchers.containsStringIgnoringCase("disabled")))
    }

    @Order(4)
    @Test
    fun disable_fail_expectException() {
        mvc.perform(get("/api/log/disable").contentType(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().isBadRequest)
            .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN))
            .andExpect(content().string(CoreMatchers.containsStringIgnoringCase("no logger currently active")))
    }
}