package at.tobiasnolz.restemployeetask.presentation.api;

import at.tobiasnolz.restemployeetask.domain.Employee;
import at.tobiasnolz.restemployeetask.domain.Task;
import at.tobiasnolz.restemployeetask.domain.exception.NoSuchEmployeeException;
import at.tobiasnolz.restemployeetask.persistence.EmployeeRepository;
import at.tobiasnolz.restemployeetask.persistence.TaskRepository;
import lombok.Getter;
import lombok.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("api/employees")
public record TaskManager(
        TaskRepository taskRepository,
        EmployeeRepository employeeRepository
) {

    @GetMapping()
    public List<Employee> getEmployees() {
        return employeeRepository.findAll();
    }

    @GetMapping("{id}")
    public Employee getEmployeeById(@PathVariable("id") String id) {
        return employeeRepository.findById(id)
                .orElseThrow(() -> new NoSuchEmployeeException(id));
    }

    @PostMapping()
    public ResponseEntity<Employee> saveEmployee(@RequestBody @Valid Employee employee) {
        var saved = employeeRepository.save(employee);
        var uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .build(saved.getId());

        return ResponseEntity
                .created(uri)
                .body(saved);
    }

    @GetMapping("{id}/hoursWorked")
    public Integer getHoursWorked(@PathVariable("id") String id) {
        return employeeRepository.getHoursWorkedByEmployeeId(id);
    }

    @GetMapping("{id}/tasks")
    public List<Task> getTasksForEmployeeInTimeRange(
            @PathVariable("id") String id,
            @RequestParam("from") LocalDate from,
            @RequestParam("to") LocalDate to) {

        return taskRepository.findAllByEmployeeIdAndFinishedBetween(id, from, to);
    }
}
