package at.tobiasnolz.restemployeetask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestEmployeeTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestEmployeeTaskApplication.class, args);
    }

}
