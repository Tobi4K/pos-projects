package at.tobiasnolz.restemployeetask.persistence;

import at.tobiasnolz.restemployeetask.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface EmployeeRepository extends JpaRepository<Employee, String> {

    @Query("""
                select sum(task.hoursWorked)
                from Task task
                where task.employee.id = :employeeId
            """)
    Integer getHoursWorkedByEmployeeId(String employeeId);
}
