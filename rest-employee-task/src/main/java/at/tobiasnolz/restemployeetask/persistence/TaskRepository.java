package at.tobiasnolz.restemployeetask.persistence;

import at.tobiasnolz.restemployeetask.domain.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;

public interface TaskRepository extends JpaRepository<Task, Integer> {

    @Query("""
            select task
            from Task task
            where task.employee.id = :id and
            task.finished between :from and :to
            """)
    List<Task> findAllByEmployeeIdAndTimeRange(String id, LocalDate from, LocalDate to);

    /* Auto generated */
    List<Task> findAllByEmployeeIdAndFinishedBetween(String id, LocalDate from, LocalDate to);
}
