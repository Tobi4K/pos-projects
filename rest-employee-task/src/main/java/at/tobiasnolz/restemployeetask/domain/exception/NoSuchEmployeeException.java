package at.tobiasnolz.restemployeetask.domain.exception;

import java.util.NoSuchElementException;

public class NoSuchEmployeeException extends NoSuchElementException {
    public NoSuchEmployeeException(String id) {
        super("No employee with id \"" + id + "\" found");
    }
}
