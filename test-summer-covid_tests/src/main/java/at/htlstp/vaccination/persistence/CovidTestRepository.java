package at.htlstp.vaccination.persistence;

import at.htlstp.vaccination.domain.CovidTest;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CovidTestRepository extends JpaRepository<CovidTest, Long> {
    List<CovidTest> findAllByPersonId(Long id);
}
