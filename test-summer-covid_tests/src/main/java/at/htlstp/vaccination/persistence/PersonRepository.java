package at.htlstp.vaccination.persistence;

import at.htlstp.vaccination.domain.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, Long> {
}
