package at.htlstp.vaccination.presentation.web;

import at.htlstp.vaccination.domain.CovidTest;
import at.htlstp.vaccination.domain.Person;
import at.htlstp.vaccination.domain.Result;
import at.htlstp.vaccination.domain.exceptions.PersonNotFoundException;
import at.htlstp.vaccination.persistence.CovidTestRepository;
import at.htlstp.vaccination.persistence.PersonRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("persons")
public record CovidTestController(PersonRepository personRepository,
                                  CovidTestRepository covidTestRepository) {

    @GetMapping()
    public String getPersonOverview(Model model) {
        var persons = personRepository.findAll();
        model.addAttribute("persons", persons);
        return "overview";
    }

    @GetMapping("{id}")
    public String getTestsForPerson(Model model,
                                    @PathVariable("id") Long personId) {
        var person = personRepository.findById(personId)
                .orElseThrow(() -> new PersonNotFoundException(personId));

        setupModel(model, person);
        model.addAttribute("covidTest", new CovidTest());

        return "testTemplate";
    }

    @PostMapping("{id}")
    public String saveNewTestForPerson(@PathVariable("id") Long personId,
                                       @Valid CovidTest covidTest,
                                       BindingResult bindingResult,
                                       Model model) {

        var person = personRepository.findById(personId)
                .orElseThrow();

        if (bindingResult.hasErrors()) {
            setupModel(model, person);
            return "testTemplate";
        }

        covidTest.setPerson(person);
        covidTestRepository.save(covidTest);
        return "redirect:/persons/%d".formatted(person.getId());
    }

    private void setupModel(Model model, Person person) {
        var tests = covidTestRepository.findAllByPersonId(person.getId());
        model.addAttribute("tests", tests);
        model.addAttribute("allResults", Result.values());
        model.addAttribute("personFromRepository", person);
    }

}
