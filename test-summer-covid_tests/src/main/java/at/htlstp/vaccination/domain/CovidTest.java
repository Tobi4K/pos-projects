package at.htlstp.vaccination.domain;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "covid_tests")

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CovidTest implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long testId;

    @NotNull(message = "Date has to be selected")
    @PastOrPresent(message = "Date may not be in the future")
    private LocalDate testDate;

    @Enumerated(EnumType.STRING)
    private Result testResult;

    @ManyToOne
    private Person person;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CovidTest covidTest = (CovidTest) o;
        return testId.equals(covidTest.testId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(testId);
    }
}
