package at.htlstp.vaccination.domain;

public enum Result {
    NEGATIVE, POSITIVE, INVALID
}
