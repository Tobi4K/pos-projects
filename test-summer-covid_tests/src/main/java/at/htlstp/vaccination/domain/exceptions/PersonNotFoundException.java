package at.htlstp.vaccination.domain.exceptions;

import java.util.NoSuchElementException;

public class PersonNotFoundException extends NoSuchElementException {
    public PersonNotFoundException(Long personId) {
        super("No person found with id \"%d\"".formatted(personId));
    }
}
