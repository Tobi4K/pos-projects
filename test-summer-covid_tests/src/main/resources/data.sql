insert into persons (last_name, first_name)
values ('Adler', 'Alfred'),
       ('Bauer', 'Bernd'),
       ('Cäsar', 'Claudia'),
       ('Dorner', 'Dominik');

insert into covid_tests (person_id, test_date, test_result)
values (1, '2022-01-13', 'NEGATIVE'),
       (2, '2022-01-13', 'POSITIVE'),
       (1, '2022-01-27', 'INVALID'),
       (4, '2022-01-20', 'NEGATIVE'),
       (1, '2022-01-27', 'POSITIVE'),
       (4, '2022-02-14', 'NEGATIVE');
