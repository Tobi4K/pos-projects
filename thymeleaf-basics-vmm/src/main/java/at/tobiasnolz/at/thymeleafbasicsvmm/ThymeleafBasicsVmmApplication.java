package at.tobiasnolz.at.thymeleafbasicsvmm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThymeleafBasicsVmmApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThymeleafBasicsVmmApplication.class, args);
    }

}
