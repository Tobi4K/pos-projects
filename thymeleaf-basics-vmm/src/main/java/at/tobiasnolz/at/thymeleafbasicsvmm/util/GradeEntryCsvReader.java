package at.tobiasnolz.at.thymeleafbasicsvmm.util;

import at.tobiasnolz.at.thymeleafbasicsvmm.domain.GradeEntry;
import at.tobiasnolz.at.thymeleafbasicsvmm.domain.Subject;
import at.tobiasnolz.at.thymeleafbasicsvmm.persistence.StudentRepository;
import com.sun.istack.logging.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class GradeEntryCsvReader {

    public static List<GradeEntry> readFromCsv(Path path, StudentRepository studentRepository) throws IOException {
        return Files.lines(path)
                .map(line -> line.split(","))
                .filter(line -> line.length == 4)
                .map(line -> {
                    var optEntry = parseLineToGradeEntry(line, studentRepository);
                    return optEntry.orElse(null);
                })
                .filter(Objects::nonNull)
                .toList();
    }

    private static Optional<GradeEntry> parseLineToGradeEntry(String[] line, StudentRepository studentRepository) {
        LocalDate assignedDate;
        int grade;
        try {
            assignedDate = LocalDate.parse(line[1]);
            grade = Integer.parseInt(line[3]);
        } catch (NumberFormatException | DateTimeParseException e) {
            Logger.getLogger(GradeEntryCsvReader.class).warning("\"" + Arrays.toString(line) + "\" could not be parsed");
            return Optional.empty();
        }

        var studentId = line[0];
        var subject = Subject.valueOf(line[2]);


        var student = studentRepository.findById(studentId);
        if (student.isPresent()) {
            return Optional.of(new GradeEntry(student.get(), assignedDate, subject, grade));
        } else {
            Logger.getLogger(GradeEntryCsvReader.class).warning("No student with studentId \"" + studentId + "\" found");
            return Optional.empty();
        }
    }
}
