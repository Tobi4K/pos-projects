package at.tobiasnolz.at.thymeleafbasicsvmm.presentation.web;

import at.tobiasnolz.at.thymeleafbasicsvmm.domain.GradeEntry;
import at.tobiasnolz.at.thymeleafbasicsvmm.domain.Subject;
import at.tobiasnolz.at.thymeleafbasicsvmm.persistence.GradeEntryRepository;
import at.tobiasnolz.at.thymeleafbasicsvmm.persistence.StudentRepository;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.NoSuchElementException;


@Controller
public record StudentOverviewController(StudentRepository studentRepository,
                                        GradeEntryRepository gradeEntryRepository) {

    @GetMapping("/overview")
    public String getOverview(Model model) {
        var students = studentRepository.findAll();
        var studentGradeEntryPair = students.stream()
                .map(student -> {
                    var gradeEntryCount = gradeEntryRepository.countAllByStudentId(student.getId());
                    return Pair.of(student, gradeEntryCount);
                })
                .toList();
        model.addAttribute("studentGradeEntryPair", studentGradeEntryPair);
        return "overview";
    }


    @GetMapping("/students/{id}")
    public String getGradesByStudent(Model model, @PathVariable("id") String id) {
        var gradeEntries = gradeEntryRepository().findAllByStudentId(id);
        var student = studentRepository.getById(id);
        var studentFullName = student.getFirstName() + " " + student.getLastName();
        model.addAttribute("gradeEntries", gradeEntries);
        model.addAttribute("pageTitle", studentFullName);
        model.addAttribute("newGradeEntry", new GradeEntry());
        model.addAttribute("subjects", Subject.values());
        return "grades-by-student";
    }

    @PostMapping("/students/{id}")
    public String addNewGradeEntry(@PathVariable("id") String studentId,
                                   @Valid @ModelAttribute("newGradeEntry") GradeEntry gradeEntry,
                                   BindingResult bindingResult,
                                   Model model) {
        var student = studentRepository.findById(studentId);
        if (student.isEmpty())
            throw new NoSuchElementException("student not found");

        if (bindingResult.hasErrors()) {
            var gradeEntries = gradeEntryRepository().findAllByStudentId(studentId);
            model.addAttribute("gradeEntries", gradeEntries);
            model.addAttribute("pageTitle", "Error");
            model.addAttribute("subjects", Subject.values());
            return "grades-by-student";
        }

        gradeEntry.setStudent(student.get());
        gradeEntryRepository.save(gradeEntry);
        return "redirect:/students/" + gradeEntry.getStudent().getId();
    }
}
