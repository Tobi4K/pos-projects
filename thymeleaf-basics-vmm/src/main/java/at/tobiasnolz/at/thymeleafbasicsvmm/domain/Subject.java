package at.tobiasnolz.at.thymeleafbasicsvmm.domain;

public enum Subject {
    POS, D, AM, E, DSAI, DBI, SYP
}
