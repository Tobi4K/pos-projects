package at.tobiasnolz.at.thymeleafbasicsvmm.persistence;

import at.tobiasnolz.at.thymeleafbasicsvmm.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, String> {
}
