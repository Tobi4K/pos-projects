package at.tobiasnolz.at.thymeleafbasicsvmm.persistence;

import at.tobiasnolz.at.thymeleafbasicsvmm.domain.GradeEntry;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GradeEntryRepository extends JpaRepository<GradeEntry, Integer> {
    Integer countAllByStudentId(String studentId);

    List<GradeEntry> findAllByStudentId(String id);
}
