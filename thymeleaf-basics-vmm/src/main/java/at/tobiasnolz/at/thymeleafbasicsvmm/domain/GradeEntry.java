package at.tobiasnolz.at.thymeleafbasicsvmm.domain;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Objects;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor

@Entity
@Table(name = "grades")
public class GradeEntry {

    @Id
    @GeneratedValue
    private Integer id;

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate assignedDate;

    @NotNull
    private Subject subject;

    @Min(1)
    @Max(5)
    private int grade;

    @ManyToOne(optional = false)
    private Student student;

    public GradeEntry(Student student, LocalDate assignedDate, Subject subject, int grade) {
        this.assignedDate = assignedDate;
        this.subject = subject;
        this.grade = grade;
        this.student = student;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GradeEntry that = (GradeEntry) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}


