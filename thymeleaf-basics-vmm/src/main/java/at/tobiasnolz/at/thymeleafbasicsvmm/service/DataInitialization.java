package at.tobiasnolz.at.thymeleafbasicsvmm.service;

import at.tobiasnolz.at.thymeleafbasicsvmm.persistence.GradeEntryRepository;
import at.tobiasnolz.at.thymeleafbasicsvmm.persistence.StudentRepository;
import at.tobiasnolz.at.thymeleafbasicsvmm.util.GradeEntryCsvReader;
import lombok.AllArgsConstructor;
import org.jboss.logging.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.Objects;


@Configuration
@AllArgsConstructor
public class DataInitialization {
    private static final String GRADES_CSV = "grades.csv";

    @Bean
    public CommandLineRunner loadData(StudentRepository studentRepository, GradeEntryRepository gradeEntryRepository) {
        return (args) -> setupData(studentRepository, gradeEntryRepository);
    }

    void setupData(StudentRepository studentRepository, GradeEntryRepository gradeEntryRepository) {
        try {
            var ressource = DataInitialization.class.getResource("/" + GRADES_CSV);
            var path = Path.of(Objects.requireNonNull(ressource).toURI());
            var gradeEntries = GradeEntryCsvReader.readFromCsv(path, studentRepository);
            gradeEntryRepository.saveAll(gradeEntries);
        } catch (URISyntaxException | IOException e) {
            var logger = Logger.getLogger(DataInitialization.class);
            logger.warn(GRADES_CSV + " could not be loaded");
            logger.info(e.getMessage());
        }
    }
}
