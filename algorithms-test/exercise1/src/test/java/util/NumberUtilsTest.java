package util;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class NumberUtilsTest {

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31})
    void isPrime_true(int primeNumber) {
        assertThat(NumberUtils.isPrime(primeNumber))
                .isTrue();
    }

    @ParameterizedTest
    @ValueSource(ints = {4, 6, 10, 20, 25, 30, 50, 66})
    void isPrime_false(int primeNumber) {
        assertThat(NumberUtils.isPrime(primeNumber))
                .isFalse();
    }

    @Test
    void minimum_success() {
        var array = Stream.of(100, 2, 3, 4, 5, 6, 7)
                .mapToInt(i -> i)
                .toArray();

        assertThat(NumberUtils.minimum(array))
                .isEqualTo(2);
    }

    @Test
    void minimum_empty_throws_exception() {
        var array = new int[]{};

        assertThrows(
                IllegalArgumentException.class,
                () -> NumberUtils.minimum(array)
        );
    }

    @Test
    void increasing_success() {
        assertAll(
                () -> assertThat(NumberUtils.increasing(12345)).isTrue(),
                () -> assertThat(NumberUtils.increasing(12233345)).isTrue(),
                () -> assertThat(NumberUtils.increasing(12354)).isFalse()
        );
    }

    @Test
    void increasing_fail_throws_exception() {
        assertThrows(
                IllegalArgumentException.class,
                () -> NumberUtils.increasing(-123)
        );
    }

    @Test
    void decreasing_success() {
        assertAll(
                () -> assertThat(NumberUtils.decreasing(54321)).isTrue(),
                () -> assertThat(NumberUtils.decreasing(5433321)).isTrue(),
                () -> assertThat(NumberUtils.decreasing(54123)).isFalse()
        );
    }

    @Test
    void decreasing_fail_throws_exception() {
        assertThrows(
                IllegalArgumentException.class,
                () -> NumberUtils.decreasing(-123)
        );
    }


    @Test
    void reverse_success() {
        assertAll(
                () -> assertThat(NumberUtils.reverse(12345)).isEqualTo(54321),
                () -> assertThat(NumberUtils.reverse(987123)).isEqualTo(321789),
                () -> assertThat(NumberUtils.reverse(1)).isEqualTo(1)
        );
    }

    @Test
    void reverse_fail_throws_exception() {
        assertAll(
                () -> assertThrows(
                        IllegalArgumentException.class,
                        () -> NumberUtils.reverse(-123)
                ),
                () -> assertThrows(
                        IllegalArgumentException.class,
                        () -> NumberUtils.reverse(10)
                )
        );
    }

    @Test
    void isBouncy() {
        assertAll(
                () -> assertThat(NumberUtils.isBouncy(12354)).isTrue(),
                () -> assertThat(NumberUtils.isBouncy(987356)).isTrue(),
                () -> assertThat(NumberUtils.isBouncy(12334)).isFalse(),
                () -> assertThat(NumberUtils.isBouncy(54321)).isFalse()
        );
    }

    @Test
    void isPalindromic() {
        assertAll(
                () -> assertThat(NumberUtils.isPalindromic(1)).isTrue(),
                () -> assertThat(NumberUtils.isPalindromic(121)).isTrue(),
                () -> assertThat(NumberUtils.isPalindromic(12344321)).isTrue(),
                () -> assertThat(NumberUtils.isPalindromic(123421)).isFalse()
        );
    }

    @Test
    void isPalindromic_fail_throws_exception() {
        assertThrows(
                IllegalArgumentException.class,
                () -> NumberUtils.decreasing(-123)
        );
    }
}