package app;

import util.NumberUtils;

import java.util.stream.IntStream;

import static util.NumberUtils.isPrime;
import static util.NumberUtils.reverse;

public class Client {
    public static void main(String[] args) {
        var primes = IntStream.rangeClosed(1, Double.valueOf(Math.pow(10, 6)).intValue())
                .boxed()
                .filter(NumberUtils::isPrime)
                .filter(number -> isPrime(reverse(number)))
                .toList();

        System.out.println("Primes found: ");
        primes.stream()
                .limit(10)
                .forEach(System.out::println);
        System.out.println("...");
        System.out.println("primes.size() = " + primes.size());

        primes.stream()
                .skip(primes.size() / 2)
                .limit(1)
                .forEach(prime -> System.out.println("median = " + prime));

    }
}
