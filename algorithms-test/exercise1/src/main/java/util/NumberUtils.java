package util;

import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class NumberUtils {

    public static boolean isPrime(long number) {
        if (number == 2)
            return true;

        if (number < 1 || number % 2 == 0) {
            return false;
        }

        return LongStream.iterate(
                3,
                divisor -> divisor <= Math.sqrt(number),
                divisor -> divisor + 2
        ).noneMatch(divisor -> number % divisor == 0);
    }

    public static int minimum(int[] array) {
        var optionalFound = Arrays.stream(array)
                .min();

        return optionalFound.orElseThrow(
                () -> new IllegalArgumentException("Could not find any integer"));
    }

    public static boolean increasing(long number) {
        checkNumberNegative(number);
        var digits = splitNumberIntoDigits(number);
        var sorted = digits.stream()
                .sorted()
                .toList();

        return digits.equals(sorted);
    }

    private static void checkNumberNegative(long number) {
        if (number < 0)
            throw new IllegalArgumentException("Number may not be negative");
    }

    private static List<Integer> splitNumberIntoDigits(long number) {
        var numberAsString = Long.valueOf(number).toString();
        return IntStream.range(0, numberAsString.length())
                .mapToObj(numberAsString::charAt)
                .map(Integer::valueOf)
                .map(i -> i - '0')
                .toList();
    }

    public static boolean decreasing(long number) {
        checkNumberNegative(number);
        var digits = splitNumberIntoDigits(number);
        var sorted = digits.stream()
                .sorted(Comparator.reverseOrder())
                .toList();

        return digits.equals(sorted);
    }

    public static long reverse(long number) {
        checkNumberNegative(number);
        if (number % 10 == 0)
            throw new IllegalArgumentException("Last digit may not be 0");

        var digits = splitNumberIntoDigits(number);
        var numberAsString = new StringBuilder();
        digits.forEach(numberAsString::append);
        return Long.parseLong(numberAsString.reverse().toString());
    }

    public static boolean isBouncy(long number) {
        return !increasing(number) && !decreasing(number);
    }

    public static boolean isPalindromic(long number) {
        return number == reverse(number);
    }
}
