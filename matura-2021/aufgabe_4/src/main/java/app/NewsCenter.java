package app;

import agency.FileLogger;
import agency.NewsAgency;
import agency.Publisher;
import agency.Subscriber;

import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class NewsCenter {
    private final static Random rd = new Random();

    private final static String[] news = {
            "Portugal: Hälfte aller CoV-Toten im Jänner",
            "Schulöffnung: Schichtbetrieb vorerst bis Osten",
            "Österreich verliert Auftakt beim ATP Cup",
            "Wachsende Sorge vor rechtem Terror in den USA",
            "Modekette Pimkie is pleite",
            "Anschober: \"Öffnungsschritte sollen Perspektive schaffen\"",
            "HTL-Schüler entwickeln Rettungsgassensimulation",
            "Biden will Einbürgerungsprozess vereinfachen",
            "Justiz fordert dreieinhalb Jahre Haft für Nawalny",
            "Grobe Datenschutzpanne in Wiener Teststraße"
    };


    public static void main(String[] args) {
        NewsAgency agency = new NewsAgency();
        var logger = new FileLogger(Path.of("news.log"));
        agency.subscribe(logger);

        for(int i = 0; i < 10; i++) {
            try {
                Thread.sleep(rd.nextInt(2000) + 1000);
            } catch (InterruptedException e) {
                System.out.println("Fail: " + e.getMessage());
            }
            agency.receiveNews(news[i]);

        }

        agency.unsubscribe(logger);
    }
}
