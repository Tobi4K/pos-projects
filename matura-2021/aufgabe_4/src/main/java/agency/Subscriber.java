package agency;

import java.io.IOException;
import java.time.LocalDateTime;

public interface Subscriber {
    void update(LocalDateTime timestamp, String news);
}
