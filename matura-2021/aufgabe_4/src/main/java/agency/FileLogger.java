package agency;

import lombok.SneakyThrows;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;

public class FileLogger implements Subscriber {

    private final Path path;

    public FileLogger(Path path) {
        this.path = path;
    }

    @Override
    @SneakyThrows
    public void update(LocalDateTime timestamp, String news) {
        var message = timestamp + ";" + news + "\n";

        if (!Files.exists(path))
            Files.createFile(path);

        Files.writeString(path, message, StandardOpenOption.APPEND);
    }
}
