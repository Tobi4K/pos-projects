package agency;

import java.time.LocalDateTime;

public interface Publisher {
    void notifySubscribers(LocalDateTime timestamp, String news);
    void subscribe(Subscriber subscriber);
    void unsubscribe(Subscriber subscriber);
}
