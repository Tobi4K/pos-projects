package agency;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class NewsAgency implements Publisher {

    private String news;

    private final List<Subscriber> subscribers = new ArrayList<>();

    public void receiveNews(String news) {
        this.news = news;
        this.notifySubscribers(LocalDateTime.now(), news);
        System.out.println("Receive new news: " + news);
    }

    @Override
    public void notifySubscribers(LocalDateTime timestamp, String news) {
        this.subscribers.forEach(subscriber -> subscriber.update(timestamp, news));
    }

    @Override
    public void subscribe(Subscriber subscriber) {
        this.subscribers.add(subscriber);
    }

    @Override
    public void unsubscribe(Subscriber subscriber) {
        this.subscribers.remove(subscriber);
    }
}
