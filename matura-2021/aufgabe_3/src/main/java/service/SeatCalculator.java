package service;

import java.util.*;

public class SeatCalculator {

    private final Set<String> parties;

    /**
     * @param parties Parteien, welche für die Wahl registriert sind, darf nicht leer sein
     */
    public SeatCalculator(Set<String> parties) {
        if (parties == null || parties.size() == 0)
            throw new IllegalArgumentException("Parties may not be null or empty");

        this.parties = parties;
    }

    /**
     * Berechnet die Sitz-/Mandatsverteilung nach D'Hondt
     *
     * @param votesPerParty für jede Partei die Anzahl an abgegeben Stimmen für diese Partei, darf nicht leer sein
     * @param seats         Anzahl zu vergebender Sitze/Mandate, muss positiv sein
     * @return für jede Partei die Anzahl ihr zugeteilter Sitze/Mandate
     */
    public Map<String, Integer> calculate(Map<String, Long> votesPerParty, int seats) {
        checkConstraints(votesPerParty, seats);
        var validVotesPerParty = getValidVotesPerParty(votesPerParty);
        return getSeatsPerPartyMap(seats, validVotesPerParty);
    }

    private LinkedHashMap<String, Integer> getSeatsPerPartyMap(int seats, HashMap<String, Long> validVotesPerParty) {
        var partySeatsMap = new LinkedHashMap<String, Integer>();
        while (seats > 0) {
            var winner = computeWinner(validVotesPerParty, partySeatsMap);
            partySeatsMap.merge(winner, 1, (oldValue, toAdd) -> oldValue += toAdd);
            seats--;
        }
        return partySeatsMap;
    }

    private String computeWinner(HashMap<String, Long> validVotesPerParty, LinkedHashMap<String, Integer> partySeatsMap) {
        var scores = new TreeMap<Double, String>(Comparator.reverseOrder());
        validVotesPerParty.forEach((key, value) -> {
            var currentSeats = partySeatsMap.getOrDefault(key, 0);
            var score = (double) value / (currentSeats + 1);
            scores.put(score, key);
        });
        var winner = scores.firstEntry().getValue();
        return winner;
    }

    private HashMap<String, Long> getValidVotesPerParty(Map<String, Long> votesPerParty) {
        var validVotesPerParty = new HashMap<>(votesPerParty);
        votesPerParty.keySet()
                .stream()
                .filter(entry -> !parties.contains(entry))
                .forEach(validVotesPerParty::remove);
        return validVotesPerParty;
    }

    private void checkConstraints(Map<String, Long> votesPerParty, int seats) {
        if (seats <= 0)
            throw new IllegalArgumentException("Seats amount may not be negative");

        if (votesPerParty == null || votesPerParty.size() == 0)
            throw new IllegalArgumentException("Votes for parties may not be of size 0 / empty");
    }
}
