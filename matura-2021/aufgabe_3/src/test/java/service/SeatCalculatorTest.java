package service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class SeatCalculatorTest {

    @Test
    void partiesMustNotBeEmpty() {
        assertThatThrownBy(() -> new SeatCalculator(null));
        assertThatThrownBy(() -> new SeatCalculator(Set.of()));
    }

    @ParameterizedTest
    @ValueSource(ints = {0, -1})
    void seatsMustBePositive(int seats) {
        var seatCalculator = new SeatCalculator(Set.of("party"));

        assertThatThrownBy(() -> seatCalculator.calculate(Map.of("party", 1L), seats));
    }

    @Test
    void votesMustNotBeEmpty() {
        var seatCalculator = new SeatCalculator(Set.of("party"));

        assertThatThrownBy(() -> seatCalculator.calculate(null, 1));
        assertThatThrownBy(() -> seatCalculator.calculate(Map.of(), 1));
    }

    @Test
    void atLeastOneValidVoteRequired() {
        var seatCalculator = new SeatCalculator(Set.of("party"));

        assertThatThrownBy(() -> seatCalculator.calculate(Map.of("invalid", 1L), 1));
    }

    @Test
    void onlyOneParty() {
        var seatCalculator = new SeatCalculator(Set.of("party"));
        var expected = Map.of("party", 2);

        assertThat(seatCalculator.calculate(Map.of(
                "party", 1L,
                "invalid", 1L),
                2))
                .containsExactlyEntriesOf(expected);
    }

    @Test
    void lessPartiesThanSeats() {
        var seatCalculator = new SeatCalculator(Set.of("CDU", "SPD", "FDP", "Grüne", "SSW"));
        var expected = new LinkedHashMap<String, Integer>();
        expected.put("CDU", 30);
        expected.put("SPD", 29);
        expected.put("FDP", 4);
        expected.put("Grüne", 4);
        expected.put("SSW", 2);

        assertThat(seatCalculator.calculate(Map.of(
                "CDU", 576_100L,
                "FDP", 94_920L,
                "SPD", 554_844L,
                "Grüne", 89_330L,
                "SSW", 51_901L),
                69))
                .containsExactlyEntriesOf(expected);
    }

    @Test
    void morePartiesThanSeats() {
        var seatCalculator = new SeatCalculator(Set.of("b", "a", "c"));
        var expected = new LinkedHashMap<String, Integer>();
        expected.put("b", 1);
        expected.put("a", 1);

        assertThat(seatCalculator.calculate(Map.of(
                "b", 4L,
                "a", 3L,
                "c", 2L),
                2))
                .containsExactlyEntriesOf(expected);
    }
}