INSERT INTO project VALUES
('B01', 'Projektportfoliomanagement-Tool'),
('B02', 'Analyse von Rettungsgassen durch KI'),
('B03', 'KI Pilz- und Vogelstimmenerkennung'),
('B04', 'Analyse und Einsatz von App- und Webbaukästen');

INSERT INTO pupil (pup_id, pup_lastname, pup_firstname, pup_project_id) VALUES
(20160597, 'Nährer', 'Paul', 'B01'),
(20160589, 'Leitner', 'Sebastian', 'B01'),
(20150182, 'Will', 'Dominik', 'B01'),
(20160596, 'Nährer', 'Felix', 'B01'),
(20160523, 'Fuchs', 'Matthias', 'B01'),
(20160510, 'Schlichting', 'Florian', 'B02'),
(20160859, 'Knapp', 'Elias', 'B02'),
(20150592, 'Pllana', 'Albin', 'B02'),
(20150210, 'Gravogl', 'Klaus', 'B02'),
(20160518, 'Hinteregger', 'Bernhard', 'B03'),
(20160554, 'Prager', 'Kilian', 'B03'),
(20160705, 'Thaler', 'Felix', 'B03'),
(20160706, 'Wintner', 'Florian', 'B03'),
(20150247, 'Kumpan', 'Eduard', 'B04'),
(20160606, 'Bandion', 'Aaron', 'B04'),
(20150148, 'Grießler', 'Gerhard', 'B04');
