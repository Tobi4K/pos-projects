package at.htlstp.rud.projektverwaltung;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjektverwaltungApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjektverwaltungApplication.class, args);
    }

}
