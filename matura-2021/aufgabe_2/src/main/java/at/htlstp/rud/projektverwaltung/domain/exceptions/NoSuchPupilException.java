package at.htlstp.rud.projektverwaltung.domain.exceptions;

import java.util.NoSuchElementException;

public class NoSuchPupilException extends NoSuchElementException {
    public NoSuchPupilException(Integer pupilId) {
        super("No pupil found with id \"" + pupilId + "\"");
    }
}
