package at.htlstp.rud.projektverwaltung.presentation.api;

import at.htlstp.rud.projektverwaltung.domain.Project;
import at.htlstp.rud.projektverwaltung.domain.Pupil;
import at.htlstp.rud.projektverwaltung.domain.Task;
import at.htlstp.rud.projektverwaltung.domain.exceptions.NoSuchProjectException;
import at.htlstp.rud.projektverwaltung.domain.exceptions.NoSuchPupilException;
import at.htlstp.rud.projektverwaltung.persistence.ProjectRepository;
import at.htlstp.rud.projektverwaltung.persistence.PupilRepository;
import at.htlstp.rud.projektverwaltung.persistence.TaskRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;

@RestController
public record ProjectManagementApiController(
        ProjectRepository projectRepository,
        TaskRepository taskRepository,
        PupilRepository pupilRepository
) {

    @GetMapping("projects")
    public List<Project> getProjects() {
        return projectRepository.findAll();
    }

    @GetMapping("pupilsbyproject/{id}")
    public List<Pupil> getStudentsByProjectId(@PathVariable("id") String projectId) {
        var project = projectRepository.findById(projectId)
                .orElseThrow(() -> new NoSuchProjectException(projectId));

        return pupilRepository.findAllByProject(project);
    }

    @PostMapping("task/{id}")
    public ResponseEntity<Task> saveTask(@PathVariable("id") Integer pupilId,
                                         @RequestBody @Valid Task task) {

        var pupil = pupilRepository.findById(pupilId)
                .orElseThrow(() -> new NoSuchPupilException(pupilId));

        task.setPupil(pupil);
        var saved = taskRepository.save(task);
        var uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("pupils/{id}")
                .build(saved.getId());
        return ResponseEntity
                .created(uri)
                .body(saved);

    }




}
