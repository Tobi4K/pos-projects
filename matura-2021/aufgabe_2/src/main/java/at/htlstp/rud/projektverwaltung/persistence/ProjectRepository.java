package at.htlstp.rud.projektverwaltung.persistence;

import at.htlstp.rud.projektverwaltung.domain.Project;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ProjectRepository extends JpaRepository<Project, String> {

}
