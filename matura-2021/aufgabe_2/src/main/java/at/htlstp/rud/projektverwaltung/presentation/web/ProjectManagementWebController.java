package at.htlstp.rud.projektverwaltung.presentation.web;

import at.htlstp.rud.projektverwaltung.domain.Task;
import at.htlstp.rud.projektverwaltung.domain.exceptions.NoSuchProjectException;
import at.htlstp.rud.projektverwaltung.persistence.ProjectRepository;
import at.htlstp.rud.projektverwaltung.persistence.PupilRepository;
import at.htlstp.rud.projektverwaltung.persistence.TaskRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
public record ProjectManagementWebController(
        ProjectRepository projectRepository,
        TaskRepository taskRepository,
        PupilRepository pupilRepository
) {

    @GetMapping("overview")
    public String getOverview(Model model) {
        model.addAttribute("projects", projectRepository.findAll());
        model.addAttribute("placeholder", "");
        return "overview";
    }

    @GetMapping("overview/projects")
    public String getProjectOverview(Model model,
                                     @RequestParam("id") String projectId) {
        var project = projectRepository.findById(projectId)
                .orElseThrow(() -> new NoSuchProjectException(projectId));

        var tasks = taskRepository.findAllByProject(project);

        model.addAttribute("projects", projectRepository.findAll());
        model.addAttribute("selected", project);
        model.addAttribute("tasks", tasks);

        return "overview";
    }

    @GetMapping("overview/projects/{projectId}/new")
    public String newTaskForm(Model model,
                              @PathVariable("projectId") String projectId) {
        setupModelForTaskForm(projectId, model);
        model.addAttribute("task", new Task());

        return "new-task";
    }

    @PostMapping("overview/projects/{projectId}/new")
    public String saveNewTask(@PathVariable("projectId") String projectId,
                              @Valid Task task,
                              BindingResult bindingResult,
                              Model model) {

        if (bindingResult.hasErrors()) {
            setupModelForTaskForm(projectId, model);
            return "new-task";
        }

        var saved = taskRepository.save(task);
        return "redirect:/overview/projects?id=" + projectId;
    }

    private void setupModelForTaskForm(String projectId, Model model) {
        var project = projectRepository.findById(projectId)
                .orElseThrow(() -> new NoSuchProjectException(projectId));

        var pupils = pupilRepository.findAllByProject(project);
        model.addAttribute("project", project);
        model.addAttribute("pupils", pupils);
    }

}
