package at.htlstp.rud.projektverwaltung.domain;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDate;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor

@Entity
@Table(name="task")
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="task_id")
    private Integer id;

    @NotBlank
    @Column(name="task_desc")
    private String description;

    @PastOrPresent
    @Column(name="task_date")
    private LocalDate date;

    @Min(10)
    @Max(300)
    @Column(name="task_duration")
    private Integer duration;

    @ManyToOne
    @JoinColumn(name="task_pupil_id")
    private Pupil pupil;
}
