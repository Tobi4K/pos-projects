package at.htlstp.rud.projektverwaltung.persistence;

import at.htlstp.rud.projektverwaltung.domain.Project;
import at.htlstp.rud.projektverwaltung.domain.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TaskRepository extends JpaRepository<Task, Integer> {
    @Query("""
        select task
        from Task task, Pupil pupil, Project project
        where project = :project and
                project = pupil.project and
                pupil = task.pupil
    """)
    List<Task> findAllByProject(Project project);
}
