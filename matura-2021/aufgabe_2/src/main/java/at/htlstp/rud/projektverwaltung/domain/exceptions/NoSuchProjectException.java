package at.htlstp.rud.projektverwaltung.domain.exceptions;

import java.util.NoSuchElementException;

public class NoSuchProjectException extends NoSuchElementException {

    public NoSuchProjectException(String projectId) {
        super("No project found with id \"" + projectId + "\"");
    }
}
