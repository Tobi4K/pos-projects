package at.htlstp.rud.projektverwaltung.domain;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor

@Entity
@Table(name = "project")
public class Project {

    @Id
    @Column(name = "pro_id", nullable = false)
    private String id;

    @Basic
    @Column(name = "pro_desc")
    private String description;

}
