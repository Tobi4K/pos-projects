package at.htlstp.rud.projektverwaltung.domain;


import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="pupil")
public class Pupil {

    @Id
    @Column(name="pup_id")
    private Integer id;

    @Column(name="pup_lastname")
    private String lastname;

    @Column(name="pup_firstname")
    private String firstname;

    @ManyToOne
    @JoinColumn(name="pup_project_id")
    private Project project;
}
