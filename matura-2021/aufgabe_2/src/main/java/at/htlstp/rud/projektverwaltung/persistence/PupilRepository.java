package at.htlstp.rud.projektverwaltung.persistence;

import at.htlstp.rud.projektverwaltung.domain.Project;
import at.htlstp.rud.projektverwaltung.domain.Pupil;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PupilRepository  extends JpaRepository<Pupil, Integer> {

    List<Pupil> findAllByProject(Project project);
}
