package util;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class BuildPlanner {

    private final Map<Character, Set<Character>> requirements = new TreeMap<>();

    public BuildPlanner(String... requirements) {
        generateRequirements(requirements);
    }

    private void generateRequirements(String[] requirements) {
        var stepPattern = Pattern.compile("Step \\w must be finished before step \\w can begin.");
        Arrays.stream(requirements)
                .filter(requirement -> requirement.matches(stepPattern.pattern()))
                .map(requirement -> requirement.split(" "))
                .filter(requirement -> requirement.length == 10)
                .map(requirement -> List.of(requirement[1].charAt(0), requirement[7].charAt(0)))
                .forEach(requirement -> {
                    var key = requirement.get(1);
                    var val = requirement.get(0);

                    if (this.requirements.containsKey(key))
                        this.requirements.get(key).add(val);
                    else {
                        var set = new HashSet<Character>();
                        set.add(val);
                        this.requirements.put(key, set);
                    }

                    this.requirements.putIfAbsent(val, new HashSet<>());
                });
    }

    public Map<Character, Set<Character>> getRequirementsForStep() {
        return requirements;
    }

    public List<Character> order() {
        var queue = new LinkedList<Character>();
        var requirementsCopy = getDeepCopyOfRequirements();

        while (queue.size() != this.requirements.size()) {
            var toAdd = getStepsWithFulfilledConstraints(queue, requirementsCopy);

            if (toAdd.size() == 0)
                throw new IllegalStateException("Cannot resolve");

            queue.addAll(toAdd);
        }

        return queue;
    }

    private ArrayList<Character> getStepsWithFulfilledConstraints(LinkedList<Character> queue,
                                                                  Map<Character, HashSet<Character>> requirementsCopy) {
        var toAdd = new ArrayList<Character>();
        for (var entry : requirementsCopy.entrySet()) {
            var key = entry.getKey();
            var val = entry.getValue();

            if (val.size() == 0 && !queue.contains(key)) {
                toAdd.add(key);
                clearFulfilledConstraint(requirementsCopy, key);
                break;
            }
        }
        return toAdd;
    }

    private Map<Character, HashSet<Character>> getDeepCopyOfRequirements() {
        return this.requirements.entrySet()
                .stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        e -> new HashSet<>(e.getValue())));
    }

    private void clearFulfilledConstraint(Map<Character, HashSet<Character>> requirementsCopy, Character key) {
        requirementsCopy
                .values()
                .forEach(val -> val.remove(key));
    }
}
